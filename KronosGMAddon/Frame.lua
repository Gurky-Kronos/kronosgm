﻿gmAddon = AceLibrary("AceAddon-2.0"):new("AceHook-2.1")

AUTO_WHISPER_COOLDOWN = 1*60 -- seconds

gmTicketSys = {};
gmTicketSys["NumberOfTickets"] = 0;
gmTicketSys["NumberOfOnlineTickets"] = 0;
gmTicketSys["newTicketInfo"] = nil;
gmTicketSys["readAllTickets"] = false;
gmTicketSys["hideTicketSystemMSG"] = false;
gmTicketSys["ticketScanID"] = 1;
gmTicketSys["ticketCurrent"] = 1;


gmCommands={};
gmCommands["ticket"] = ".ticket"
gmCommands["ticket respond"] = ".ticket respond"
gmCommands["kick"] = ".kick"
gmCommands["Mute"] = ".mute"
gmCommands["Ban"] = ".ban character"
gmCommands["BanInfo"] = ".baninfo character"
gmCommands["BanIP"] = ".ban ip"
gmCommands["Lookup"] = ".lookup player ip"
gmCommands["Kick"] = ".kick"
gmCommands["gmon"] = ".gm on"
gmCommands["gmoff"] = ".gm off"
gmCommands["gmvon"] = ".gm v on"
gmCommands["gmvoff"] = ".gm v off"
gmCommands["whispon"] = ".whisper on"
gmCommands["whispoff"] = ".whisper off"
gmCommands["speed"] = ".mod speed"
gmCommands["setview"] = ".gm setview"
gmCommands["die"] = ".die"
gmCommands["respawn"] = ".respawn"

gmCommands["kodoquest"] = ".q c 5561"


gmCommands["ticket delete"] = ".delticket"
gmCommands["pinfo"] = ".pinfo"
gmCommands["summon"] = ".namego"
gmCommands["teleportTo"] = ".goname"
gmCommands["recall"] = ".recall"
gmCommands["send items"] = ".send items"
gmCommands["send mail"] = ".send mail"
gmCommands["axes"] = ".learn 196"
gmCommands["axestwo"] = ".learn 197"
gmCommands["maces"] = ".learn 198"
gmCommands["macestwo"] = ".learn 199"
gmCommands["polearms"] = ".learn 200"
gmCommands["swords"] = ".learn 201"
gmCommands["swordstwo"] = ".learn 202"
gmCommands["unarmed"] = ".learn 203"
gmCommands["defense"] = ".learn 204"
gmCommands["staves"] = ".learn 227"
gmCommands["bows"] = ".learn 264"
gmCommands["guns"] = ".learn 266"
gmCommands["daggers"] = ".learn 1180"
gmCommands["Thrown"] = ".learn 2567"
gmCommands["wands"] = ".learn 5009"
gmCommands["crossbows"] = ".learn 5011"
gmCommands["fistweapons"] = ".learn 15590"

gmCommands["setaxes"] = ".setskill 44 300 300"
gmCommands["setaxestwo"] = ".setskill 172 300 300"
gmCommands["setmaces"] = ".setskill 54 300 300"
gmCommands["setmacestwo"] = ".setskill 160 300 300"
gmCommands["setpolearms"] = ".setskill 229 300 300"
gmCommands["setswords"] = ".setskill 43 300 300"
gmCommands["setswordstwo"] = ".setskill 55 300 300"
gmCommands["setunarmed"] = ".setskill 162 300 300"
gmCommands["setdefense"] = ".setskill 95 300 300"
gmCommands["setstaves"] = ".setskill 136 300 300"
gmCommands["setbows"] = ".setskill 45 300 300"
gmCommands["setguns"] = ".setskill 46 300 300"
gmCommands["setdaggers"] = ".setskill 173 300 300"
gmCommands["setThrown"] = ".setskill 176 300 300"
gmCommands["setwands"] = ".setskill 228 300 300"
gmCommands["setcrossbows"] = ".setskill 226 300 300"
gmCommands["setfistweapons"] = ".setskill 473 300 300"

gmCommands["Common"] = ".learn 668"
gmCommands["Orcish"] = ".learn 669"
gmCommands["Taurahe"] = ".learn 670"
gmCommands["Darnassian"] = ".learn 671"
gmCommands["Dwarven"] = ".learn 672"
gmCommands["Thalassian"] = ".learn 813"
gmCommands["Draconic"] = ".learn 814"
gmCommands["Demon Tongue"] = ".learn 815"
gmCommands["Titan"] = ".learn 816"
gmCommands["Old Tongue"] = ".learn 817"
gmCommands["Gnomish"] = ".learn 7340"
gmCommands["Troll"] = ".learn 7341"
gmCommands["Gutterspeak"] = ".learn 17737"

gmCommands["Sheilds"] = ".learn 9116"
gmCommands["Cloth"] = ".learn 9078"
gmCommands["Leather"] = ".learn 9077"
gmCommands["Mail"] = ".learn 8737"
gmCommands["Plate"] = ".learn 750"

gmCommands["Org"] = ".t Orgri"
gmCommands["IF"] = ".t Ironforge"
gmCommands["SW"] = ".t Storm"
gmCommands["UC"] = ".t Undercity"
gmCommands["Darn"] = ".t Darnas"
gmCommands["TB"] = ".t Thunderbluff"
gmCommands["GM"] = ".t gm"
gmCommands["Jail"] = ".go xyz 16219 16403 -64.379 1"
gmCommands["Dev"] = ".go xyz 16303 -16173 45 451"
gmCommands["Pro"] = ".go xyz 16304 16318 75 451"
gmCommands["AC"] = ".t crater"
gmCommands["Hyj"] = ".go 4674.88 -3638.37 966.264 1"
gmCommands["Death"] = ".t deathknel"
gmCommands["Mul"] = ".go -2907 -260 53 1"
gmCommands["Vot"] = ".t valleyoftrials"
gmCommands["Shadow"] = ".t shadowglen"
gmCommands["North"] = ".t northshire"
gmCommands["Cold"] = ".t coldridge"
gmCommands["Kazzak"] = ".go -12226 -2484 2 0"
gmCommands["Azuregos"] = ".go 2283 -6061 110 1"
gmCommands["Lethon"] = ".go 725 -4021 98 0"
gmCommands["Emeriss"] = ".go -2927 1908 35 1"
gmCommands["Ysondre"] = ".go 3172 -3718 124 1"
gmCommands["Taerar"] = ".go -10451 -423 44 0"

gmCommands["Speed5"] = ".m s 5"

gmCommands["Undercloft"] = ".go 1574 -3266 90 0"
gmCommands["Eliza"] = ".go -10277 54 44 0"
gmCommands["Defiler"] = ".go -11232 -2838 165 0"
gmCommands["Taelan"] = ".go 2937 -1399 170 0"
gmCommands["TaelanEvent"] = ".go 2665 -1897 75 0"
gmCommands["KodoGraveyard"] = ".go -1296 1963 70 1"
gmCommands["Quagmire"] = ".go -4025 -3370 45 1"
gmCommands["DashelStonefist"] = ".go -8679.904297 438.315460 100 0"
gmCommands["Dragons"] = ".go -6704.650391 -4135.619629 270 0"


gmCommands["Tale"] = ".reset talents"
gmCommands["Stat"] = ".reset stats"

gmCommands["Alch"] = ".learn 11611"
gmCommands["Blac"] = ".learn 9785"
gmCommands["Ench"] = ".learn 13920"
gmCommands["Engi"] = ".learn 12656"
gmCommands["Herb"] = ".learn 11993"
gmCommands["Leat"] = ".learn 10662"
gmCommands["Ridi"] = ".learn 33391"
gmCommands["Skin"] = ".learn 10768"
gmCommands["Tail"] = ".learn 12180"
gmCommands["Cook"] = ".learn 18260"
gmCommands["Firs"] = ".learn 10846"
gmCommands["Fish"] = ".learn 18248"
gmCommands["Mini"] = ".learn 10248"
gmCommands["BlastWave"] = ".learn 23331"
gmCommands["DarkGlare"] = ".learn 26029"
gmCommands["MagnetPull"] = ".learn 28337"
gmCommands["Knockback"] = ".learn 11027"
gmCommands["Distract"] = ".learn 1725"



gmCommands["setAlch"] = ".setskill 171 300 300"
gmCommands["setBlac"] = ".setskill 164 300 300"
gmCommands["setEnch"] = ".setskill 333 300 300"
gmCommands["setEngi"] = ".setskill 202 300 300"
gmCommands["setHerb"] = ".setskill 182 300 300"
gmCommands["setLeat"] = ".setskill 165 300 300"
gmCommands["setRidi"] = ".setskill 762 150 150"
gmCommands["setSkin"] = ".setskill 393 300 300"
gmCommands["setTail"] = ".setskill 197 300 300"
gmCommands["setCook"] = ".setskill 185 300 300"
gmCommands["setFirs"] = ".setskill 129 300 300"
gmCommands["setFish"] = ".setskill 356 300 300"
gmCommands["setMini"] = ".setskill 186 300 300"

gmCommands["PirateMale"] = ".aura 24708"
gmCommands["PirateFemale"] = ".aura 24709"
gmCommands["NinjaMale"] = ".aura 24711"
gmCommands["NinjaFemale"] = ".aura 24710"
gmCommands["LeperGnome"] = ".aura 24712"
gmCommands["Skeleton"] = ".aura 24723"
gmCommands["GhostMale"] = ".aura 24735"
gmCommands["GhostFemale"] = ".aura 24736"
gmCommands["RedOgre"] = ".aura 30167"
gmCommands["StunForever"] = ".aura 23775"
gmCommands["UnStunForever"] = ".unaura 23775"
gmCommands["Freeze"] = ".aura 9454"
gmCommands["UnFreeze"] = ".unaura 9454"

function GmAddonW(Text, Prijemce) -- TODO Finished by Doctorbeefy/Gurky/Bennylava
	if strlen(Text) > 255 and strlen(Text) <= 500 then
		SendChatMessage(strsub(Text,0,250),"WHISPER", nil, Prijemce);
		SendChatMessage(strsub(Text,251,strlen(Text)),"WHISPER", nil, Prijemce);
	elseif strlen(Text) > 500 and strlen(Text) < 750 then
		SendChatMessage(strsub(Text,0,250),"WHISPER", nil, Prijemce);
		SendChatMessage(strsub(Text,251,500),"WHISPER", nil, Prijemce);
		SendChatMessage(strsub(Text,501,strlen(Text)),"WHISPER", nil, Prijemce);
	else	
		SendChatMessage(Text,"WHISPER", nil, Prijemce);
	end
end


 function print(text)  
	 gmAddonPrint(text,1, 1, 1, "gmAddon");
 end

function gmAddon:OnInitialize()
	self:Hook(DEFAULT_CHAT_FRAME, "AddMessage")
end

function gmAddon:AddMessage(frame, text, r, g, b, id)
	if id == GetChatTypeIndex("SYSTEM") then
		if strfind(text,"Tickets count: %d+") then -- Load the number of tickets
			_,_,gmTicketSys["NumberOfTickets"],gmTicketSys["newTicketInfo"] = strfind(text,"Tickets count: (%d+) show new tickets: (%w+)")
			gmTicketSys["NumberOfTickets"] = tonumber(gmTicketSys["NumberOfTickets"])
			if gmTicketSys["readAllTickets"] then

				
				ticketArray = {};
				gmUpdateSlider()
				for i = 1,gmTicketSys["NumberOfTickets"] do
					gmTicketSys["ticketScanID"] = i
					SendChatMessage(gmCommands["ticket"].." "..gmTicketSys["ticketScanID"])
				end
				gmTicketSys["NumberOfTickets"] = 0 -- number of ticket obtained during ticket reading
				gmTicketSys["readAllTickets"]  = false
			end

			if gmTicketSys["hideTicketSystemMSG"] then return end



			
		elseif ticketArray and strfind(text,"New ticket from %w+") then -- Read new Tickets
			local _,_, playerName = strfind(text,"New ticket from (%w+)");
			SendChatMessage(".ticket "..playerName)
			gmAddonReadTicket = GetTime()
			PlaySound("PVPTHROUGHQUEUE" ,"master")
			
			gmNewTicket = true
			

			


		elseif ticketArray and strfind(text,"Ticket of ") and strfind(text,"Last updated") then -- Read the ticket
			local _,_,playerName = string.find(arg1, "player:(%w+)")
			local _,a,y,mm,d,h,m,s = string.find(arg1, "Last updated: (%d+)-(%d+)-(%d+)_(%d+)-(%d+)-(%d+)")
			msg = strsub(text,a+3,strlen(text))
			-- gmAddonReadTicket = GetTime()
			t = Ticket:new()
			t:setPlayerName(playerName)
			t:setTicketMsg(msg)
			t:setTicketTime(h..":"..m)
			t:setTicketDateA(mm.."."..d.."."..y)
			-- t:setTicketDateA(d.."."..mm..".")

			if (gmNewTicket) then 
				t:setTicketOnline(true) 
				gmTicketSys["NumberOfOnlineTickets"] = gmTicketSys["NumberOfOnlineTickets"] + 1
				gmNewTicket = nil
			end

			tinsert(ticketArray,t);
			

			gmTicketSys["NumberOfTickets"] = gmTicketSys["NumberOfTickets"] + 1
			updateBtns()
			gmUpdateSlider()
			if gmTicketSys["hideTicketSystemMSG"] then return end
			


			
		elseif ticketArray and id == GetChatTypeIndex("SYSTEM") and msg and gmAddonReadTicket then -- multi line ticket (max 1sec to read all lines)
			t:addTicketMsg(text)
			-- gmStopReadingTickets()

			if gmTicketSys["hideTicketSystemMSG"] then return end


		elseif ticketArray and strfind(text,"Character") and strfind(text,"ticket deleted") then -- Delete ticket
			local _,_,playerName = string.find(arg1, "player:(%w+)")


			gmDeleteTicket(playerName)

		elseif ticketArray and gmCheckOnline:GetChecked() and string.find(text, "Level %d+ %a+ %a+") and string.find(text, "player")  then
			
			local _,_,PlayerChat = string.find(text, "player:(%w+)")
			if gmCheckOnlinePlayerName == PlayerChat then 
				table.foreachi(ticketArray, function(k,v)
					if v.playerName == PlayerChat then
						v:setTicketOnline(true)
						gmTicketSys["NumberOfOnlineTickets"] = gmTicketSys["NumberOfOnlineTickets"] + 1
						-- gmUpdateSlider()
						updateBtns()
					end
				end)
				return
			end
			
			

			
		elseif ticketArray and gmCheckOnline:GetChecked() and  string.find(text, "%d+ playe%a+ total") and gmCheckOnlineTicketTime + 0.2 > GetTime() then -- number of players found (SendWho(...))
			updateBtns()
			return
		end

	end
	
	if id == null then id = "-" end
	-- self.hooks[frame].AddMessage(frame, string.format("%s", "["..id.."] "..text), r, g, b, id);
	self.hooks[frame].AddMessage(frame, string.format("%s", text), r, g, b, id);
	
	
end
function gmDeleteTicket(playerName)
	table.foreachi(ticketArray, function(k,v)
		-- print(k.." "..v.playerName)
		if (v and v.playerName == playerName) then
			-- print(k.." "..v.playerName)
			tremove(ticketArray,k)
			gmTicketSys["NumberOfTickets"] = gmTicketSys["NumberOfTickets"] - 1
			if v.TicketOnline then gmTicketSys["NumberOfOnlineTickets"] = gmTicketSys["NumberOfOnlineTickets"] - 1 end
			
			updateBtns()
			gmUpdateSlider()
			if k == gmTicketSys["ticketCurrent"] then 
				btnGmNextT_OnClick()
			elseif k<gmTicketSys["ticketCurrent"] then 
				btnGmPrevT_OnClick()
			end
			-- return
		end

	end)
end

function gmStopReadingTickets()
	if getn(ticketArray) == gmTicketSys["NumberOfTickets"] then 
		-- print("STOP")
		
		gmTicketSys["readAllTickets"] = false;
		gmTicketSys["hideTicketSystemMSG"] = false

		-- if gmPlayerName:GetText() == ""  then gmSetTicket(1) end
	end
end

function gmReadAllTickets()
	SendChatMessage(gmCommands["ticket"])
	gmTicketSys["readAllTickets"] = true;
	gmTicketSys["hideTicketSystemMSG"] = true
end


Ticket = {
	playerName = "",
	ticketMsg = "",
	ticketTime = "",
	ticketDateA = "",
	ticketDateB = "",
	answeredBy = "",
	TicketOnline = false,

	setPlayerName = function (self,str)
		self.playerName = str;
	end,

	setTicketMsg = function (self,str)
		self.ticketMsg = str;
	end,

	addTicketMsg = function (self,str)
		self.ticketMsg = self.ticketMsg.."\n "..str;
	end,

	setTicketTime = function (self,str)
		self.ticketTime = str;
	end,

	setTicketDateA = function (self,str)
		self.ticketDateA = str;
	end,

	setTicketDateB = function (self,str)
		self.ticketDateB = str;
	end,

	setTicketOnline = function (self,bool)
		self.TicketOnline = bool;
	end,

	--setAnsweredBy = function (self,str)
	--	self.answeredBy = str;
	--end,
}



function Ticket:new(o)
	o = o or {}   -- create object if user does not provide one
	setmetatable(o, self)
	self.__index = self
	return o
end



function gmAddonFrame_OnLoad()
	gmAddonFrame:RegisterForDrag("LeftButton");

	gmAddonFrame:SetHeight(550)
	gmAddonFrame:SetWidth(520)
	txtGmMsgT:Hide()
	txtGmTTime:Hide()
	gmAssign:Show()
	gmTCount:Hide()
	gmJokeOpen:Disable()
	btnGmPrevT:Disable()
	btnGmNextT:Disable()
	btnGmFirst:Disable()
	btnGmLast:Disable()
	btnGmDeleteT:Disable()
	btnGmDeleteT:Disable()
	gmCheckOnline:Disable()
	btnGmRespond:Disable()
	btnGmRefesh:Disable()
	btnGmwhisp2:Disable() --Start conversation button
	btnGmwhisp3:Hide() --old preset button2
	btnGmwhisp4:Hide()	--old preset button3
	btnGmwhisp5:Hide()	--old preset button4
	btnGmwhisp6:Hide()	--old preset button5	
	btnGmwhisp7:Hide()	--old preset button6
	btnGmEscalate:Disable()
	btnGmEscalate2:Disable()
	btnGmHoldTicket:Disable()
	gmTicketBorderFrame:Hide()
	GmQuestLog:Hide()
	btngmTeleEliza:Hide()
	btngmTeleKodoGraveyard:Hide()
	btngmDashel:Hide()
	btngmTeleTaelanEvent:Hide()
	btngmTeleDragons:Hide()
	btngmTeleUndercroft:Hide()
--
	
	if not MySlider then
		CreateFrame('Slider', 'MySlider', gmAddonFrame, 'OptionsSliderTemplate')
	end

	MySlider:ClearAllPoints()


	
	getglobal(MySlider:GetName() .. 'Low'):SetText('');
	
	MySlider:SetOrientation('VERTICAL')

-- Position the slider between gmAddonFrame and Ticket Buttons

	getglobal(MySlider:GetName() .. 'High'):SetText('100');
	MySlider:SetMinMaxValues(1, 100)
	MySlider:SetValueStep(10)
	MySlider:SetHeight(523)
	MySlider:SetWidth(13)
	MySlider:SetPoint("TopLeft", gmAddonFrame,"TopLeft", -10, -9)
	MySlider:Hide()
	MySlider:SetScript("OnValueChanged", updateBtns)
	-- MySlider:SetScript("OnMouseWheel", gmTickets_OnMouseWheel(arg1,arg2))
	-- MySlider:SetScript("OnMinMaxChanged", MyFunction)

	

	---- 

	txtGmMsgT:SetBackdrop({
		bgFile = [[Interface\Buttons\WHITE8x8]],
		insets = {left = -5, right = 0, top = 0, bottom = 0
		}})
	txtGmMsgT:SetBackdropColor(94, 0.88, 0.19, 0.1)
	txtGmMsgT:SetHeight(100)
	txtGmMsgT:SetFont("Fonts\\FRIZQT__.TTF", 14, "OUTLINE, THICKOUTLINE")
	txtGmMsgT:SetTextColor(1.0, 0.65, 0.0);

	txtGmSendWhisp:SetBackdrop({
		bgFile = [[Interface\Buttons\WHITE8x8]],
		insets = {left = -5, right = 0, top = 0, bottom = 0
		}})
	txtGmSendWhisp:SetBackdropColor(0, 0, 0, 0.5)
	txtGmSendWhisp:SetHeight(13)


	btn = {}
-- gmBtn1T:EnableMouseWheel(true);
-- gmBtn1T:SetScript("OnMouseWheel", function(self,delta) gmBtn1T:SetText(tostring(delta)) end)
	tinsert(btn,gmBtn1T)
	gmBtn1T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn1T:GetText()) end)
	tinsert(btn,gmBtn2T)
	gmBtn2T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn2T:GetText()) end)
	tinsert(btn,gmBtn3T)
	gmBtn3T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn3T:GetText()) end)
	tinsert(btn,gmBtn4T)
	gmBtn4T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn4T:GetText()) end)
	tinsert(btn,gmBtn5T)
	gmBtn5T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn5T:GetText()) end)
	tinsert(btn,gmBtn6T)
	gmBtn6T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn6T:GetText()) end)
	tinsert(btn,gmBtn7T)
	gmBtn7T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn7T:GetText()) end)
	tinsert(btn,gmBtn8T)
	gmBtn8T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn8T:GetText()) end)
	tinsert(btn,gmBtn9T)
	gmBtn9T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn9T:GetText()) end)
	tinsert(btn,gmBtn10T)
	gmBtn10T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn10T:GetText()) end)
	tinsert(btn,gmBtn11T)
	gmBtn11T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn11T:GetText()) end)
	tinsert(btn,gmBtn12T)
	gmBtn12T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn12T:GetText()) end)
	tinsert(btn,gmBtn13T)
	gmBtn13T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn13T:GetText()) end)
	tinsert(btn,gmBtn14T)
	gmBtn14T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn14T:GetText()) end)
	tinsert(btn,gmBtn15T)
	gmBtn15T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn15T:GetText()) end)
	tinsert(btn,gmBtn16T)
	gmBtn16T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn16T:GetText()) end)
	tinsert(btn,gmBtn17T)
	gmBtn17T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn17T:GetText()) end)
	tinsert(btn,gmBtn18T)
	gmBtn18T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn18T:GetText()) end)
	tinsert(btn,gmBtn19T)
	gmBtn19T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn19T:GetText()) end)
	tinsert(btn,gmBtn20T)
	gmBtn20T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn20T:GetText()) end)
	tinsert(btn,gmBtn21T)
	gmBtn21T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn21T:GetText()) end)
	tinsert(btn,gmBtn22T)
	gmBtn22T:SetScript("OnClick", function(self, button, down) setTicketByPlayerName(gmBtn22T:GetText()) end)
	--simTickets()
	
	gmAddonFrame:RegisterEvent("CHAT_MSG_ADDON");
	gmAddonFrame:RegisterEvent("CHAT_MSG_WHISPER");
end
 function mouse(self,delta)
	 gmBtn1T:SetText(tostring(delta))
 end

function setTicketByPlayerName(name)
	-- print(name)
	name = gsub(name, "|cff20ff20", "")
	name = gsub(name, "|cff808080", "")
	table.foreachi(ticketArray, function(k,v)
		if (v and v.playerName == name) then
			gmSetTicket(k)
		end

	end)

end

function btnGmLoadT_OnClick()
	GuildRoster() -- fetch info about guild members, needed to update GM's public note.

	gmAddonFrame:SetHeight(550)
	gmAddonFrame:SetWidth(520)
	txtGmSendWhisp:Show()
	txtGmMsgT:Show()
	gmTCount:Show()
	gmAssign:Show()
	txtGmTTime:Show()
	gmJokeOpen:Enable()
	btnGmPrevT:Enable()
	btnGmNextT:Enable()
	btnGmFirst:Enable()
	btnGmLast:Enable()
	btnGmDeleteT:Enable()
	gmCheckOnline:Enable()
	btnGmRespond:Enable()
	btnGmLoadT:Hide()
	btnGmRefesh:Enable()
	btnGmwhisp2:Enable()
	btnGmwhisp3:Hide()	--old greeting preset button
	btnGmwhisp4:Hide()	--old greeting preset button
	btnGmwhisp5:Hide()	--old greeting preset button
	btnGmwhisp6:Hide()	--old greeting preset button
	btnGmwhisp7:Hide() 	--old greeting preset button
	btnGmEscalate:Enable()
	btnGmEscalate2:Enable()
	btnGmHoldTicket:Enable()
	gmTicketBorderFrame:Show()	
	gmStartSessionFrame:Hide()
	GmQuestLog:Show()
	btngmTeleEliza:Show()
	btngmTeleKodoGraveyard:Show()
	btngmDashel:Show()	
	btngmTeleTaelanEvent:Show()
	btngmTeleDragons:Show()
	btngmTeleUndercroft:Show()	
	

	ticketArray = {};
	gmAddonReadTicket = GetTime()
	gmTicketSys["NumberOfTickets"] = 0
	gmTicketSys["NumberOfOnlineTickets"] = 0
	gmPlayerName:SetText("")
	gmJokePlayer:SetText("")	
	gmCommonPlayer:SetText("")	
	txtGmMsgT:SetText("Updating tickets... If this takes too long type /reload and load the addon again. Or click the 'Refresh Ticket Text' button!")
	txtGmTTime:SetText("")
	gmReadAllTickets()



	
end

function btnGmNextT_OnClick()
	if gmTicketSys["ticketCurrent"] + 1 > getn(ticketArray) then
		gmTicketSys["ticketCurrent"] = 1
	else
		gmTicketSys["ticketCurrent"] = gmTicketSys["ticketCurrent"] + 1
	end
	gmSetTicket(gmTicketSys["ticketCurrent"])
	MySlider:SetValue(gmTicketSys["ticketCurrent"])

end

function btnGmPrevT_OnClick()
	if gmTicketSys["ticketCurrent"] - 1 <= 0  then
		gmTicketSys["ticketCurrent"] = getn(ticketArray)
	else
		gmTicketSys["ticketCurrent"] = gmTicketSys["ticketCurrent"] - 1
	end
	gmSetTicket(gmTicketSys["ticketCurrent"])
	MySlider:SetValue(gmTicketSys["ticketCurrent"])

end


function btnGmFirst_OnClick()
	gmSetTicket(1)
	MySlider:SetValue(gmTicketSys["ticketCurrent"])
end

function btnGmLast_OnClick()
	gmSetTicket(getn(ticketArray))
	MySlider:SetValue(gmTicketSys["ticketCurrent"])
end

function btnGmDeleteT_OnClick()
	if strlower(gmPlayerName:GetText()) == "all" then return end
	SendChatMessage(gmCommands["ticket delete"].." "..gmPlayerName:GetText())
	gmAddTicketSolved()
	gmSendAddonMsg("Character "..gmPlayerName:GetText().." ticket deleted")
	
end

function btnGmPinfo_OnClick()
	SendChatMessage(gmCommands["pinfo"].." "..gmPlayerName:GetText())
end

function btnGmTinfo_OnClick()
	gmPrintGuildNotes()
end

function btnGmTinfoR_OnClick()
	gmResetPublicNote()
end


function btngmLearnwep_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["axes"])
	SendChatMessage(gmCommands["axestwo"])
	SendChatMessage(gmCommands["maces"])
	SendChatMessage(gmCommands["macestwo"])
	SendChatMessage(gmCommands["polearms"])
	SendChatMessage(gmCommands["swords"])
	SendChatMessage(gmCommands["swordstwo"])
	SendChatMessage(gmCommands["unarmed"])
	SendChatMessage(gmCommands["defense"])
	SendChatMessage(gmCommands["staves"])
	SendChatMessage(gmCommands["bows"])
	SendChatMessage(gmCommands["guns"])
	SendChatMessage(gmCommands["daggers"])
	SendChatMessage(gmCommands["Thrown"])
	SendChatMessage(gmCommands["wands"])
	SendChatMessage(gmCommands["crossbows"])
	SendChatMessage(gmCommands["fistweapons"])
	SendChatMessage(gmCommands["setaxes"])
	SendChatMessage(gmCommands["setaxestwo"])
	SendChatMessage(gmCommands["setmaces"])
	SendChatMessage(gmCommands["setmacestwo"])
	SendChatMessage(gmCommands["setpolearms"])
	SendChatMessage(gmCommands["setswords"])
	SendChatMessage(gmCommands["setswordstwo"])
	SendChatMessage(gmCommands["setunarmed"])
	SendChatMessage(gmCommands["setdefense"])
	SendChatMessage(gmCommands["setstaves"])
	SendChatMessage(gmCommands["setbows"])
	SendChatMessage(gmCommands["setguns"])
	SendChatMessage(gmCommands["setdaggers"])
	SendChatMessage(gmCommands["setThrown"])
	SendChatMessage(gmCommands["setwands"])
	SendChatMessage(gmCommands["setcrossbows"])
	SendChatMessage(gmCommands["setfistweapons"])	

	end
	

end

function btngmLearnLang_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Common"])
	SendChatMessage(gmCommands["Orcish"])
	SendChatMessage(gmCommands["Taurahe"])
	SendChatMessage(gmCommands["Darnassian"])
	SendChatMessage(gmCommands["Dwarven"])
	SendChatMessage(gmCommands["Thalassian"])
	SendChatMessage(gmCommands["Draconic"])
	SendChatMessage(gmCommands["Demon Tongue"])
	SendChatMessage(gmCommands["Titan"])
	SendChatMessage(gmCommands["Old Tongue"])
	SendChatMessage(gmCommands["Gnomish"])
	SendChatMessage(gmCommands["Troll"])
	SendChatMessage(gmCommands["Gutterspeak"])

	end	
end	

function btngmLearnGM_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["MagnetPull"])
	SendChatMessage(gmCommands["Knockback"])
	SendChatMessage(gmCommands["Distract"])

	end	
end	

gmCommands["BlastWave"] = ".learn 23331"
gmCommands["DarkGlare"] = ".learn 26029"
gmCommands["MagnetPull"] = ".learn 28337"
gmCommands["Knockback"] = ".learn 11027"
gmCommands["Distract"] = ".learn 1725"

function btngmLearnTale_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Tale"])


	end	
end	

function btngmLearnStat_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Stat"])


	end	
end	

function btngmLearnAlch_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Alch"])
	SendChatMessage(gmCommands["setAlch"])	

	end	
end	

function btngmLearnBlac_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Blac"])
	SendChatMessage(gmCommands["setBlac"])	

	end	
end	

function btngmLearnEnch_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Ench"])
	SendChatMessage(gmCommands["setEnch"])	

	end	
end	

function btngmLearnEngi_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Engi"])
	SendChatMessage(gmCommands["setEngi"])	

	end	
end	

function btngmLearnHerb_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Herb"])
	SendChatMessage(gmCommands["setHerb"])

	end	
end	

function btngmLearnLeat_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Leat"])
	SendChatMessage(gmCommands["setLeat"])

	end	
end	

function btngmLearnRidi_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Ridi"])
	SendChatMessage(gmCommands["setRidi"])

	end	
end	

function btngmLearnSkin_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Skin"])
	SendChatMessage(gmCommands["setSkin"])

	end	
end	

function btngmLearnTail_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Tail"])
	SendChatMessage(gmCommands["setTail"])

	end	
end	

function btngmLearnCook_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Cook"])
	SendChatMessage(gmCommands["setCook"])

	end	
end	

function btngmLearnFirs_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Firs"])
	SendChatMessage(gmCommands["setFirs"])
	end	
end	

function btngmLearnFish_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Fish"])
	SendChatMessage(gmCommands["setFish"])
	end	
end	

function btngmLearnMini_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Mini"])
	SendChatMessage(gmCommands["setMini"])	
	end	
end	

function btngmLearnArm_OnClick()

	if ( UnitIsUnit("target", "player") ) then	
	SendChatMessage(gmCommands["Sheilds"])

	end	
end	


--Ilmus started adding Maintenance Frame tele commands
function btngmTeleUndercloft_OnClick()

	SendChatMessage(gmCommands["Undercloft"])
end	

function btngmTeleEliza_OnClick()

	SendChatMessage(gmCommands["Eliza"])
	TargetByName("Eliza")	
end	

function btngmTeleDefiler_OnClick()

	SendChatMessage(gmCommands["Defiler"])
end	

function btngmTeleKodoGraveyard_OnClick()

	-- SendChatMessage(gmCommands["KodoGraveyard"])
	SendChatMessage(gmCommands["teleportTo"])	
	TargetByName(gmPlayerName:GetText())	
	SendChatMessage(gmCommands["kodoquest"])	
	
end	

function btngmTeleQuagmire_OnClick()

	SendChatMessage(gmCommands["Quagmire"])
end	

function btngmTeleDashelStonefist_OnClick()

	SendChatMessage(gmCommands["DashelStonefist"])
	TargetByName("DashelStonefist")
	SendChatMessage(gmCommands["die"])
	SendChatMessage(gmCommands["respawn"])	
end	

function btngmTeleDragons_OnClick()

	SendChatMessage(gmCommands["Dragons"])
end	

function btngmTeleTaelan_OnClick()

	SendChatMessage(gmCommands["Taelan"])
end	

function btngmTeleTaelanEvent_OnClick()

	SendChatMessage(gmCommands["TaelanEvent"])
end	
--Ilmus stopped adding Maintenance Frame tele commands

function btngmTeleOrg_OnClick()

	SendChatMessage(gmCommands["Org"])
end	

function btngmTeleUC_OnClick()

	SendChatMessage(gmCommands["UC"])
end	

function btngmTeleTB_OnClick()

	SendChatMessage(gmCommands["TB"])
end	

function btngmTeleDeath_OnClick()

	SendChatMessage(gmCommands["Death"])
end	

function btngmTeleMul_OnClick()

	SendChatMessage(gmCommands["Mul"])
end	

function btngmTeleVot_OnClick()

	SendChatMessage(gmCommands["Vot"])
end	


function btngmTeleIf_OnClick()

	SendChatMessage(gmCommands["IF"])

end	

function btngmTeleSW_OnClick()

	SendChatMessage(gmCommands["SW"])

end	

function btngmTeleDarn_OnClick()

	SendChatMessage(gmCommands["Darn"])

end	

function btngmTeleShadow_OnClick()

	SendChatMessage(gmCommands["Shadow"])

end	

function btngmTeleNorth_OnClick()

	SendChatMessage(gmCommands["North"])

end	

function btngmTeleCold_OnClick()

	SendChatMessage(gmCommands["Cold"])

end	

function btngmTeleGM_OnClick()

	SendChatMessage(gmCommands["GM"])

end	

function btngmTeleJail_OnClick()

	SendChatMessage(gmCommands["Jail"])

end	

function btngmTeleDev_OnClick()

	SendChatMessage(gmCommands["Dev"])

end	

function btngmTelePro_OnClick()

	SendChatMessage(gmCommands["Pro"])

end	

function btngmTeleAC_OnClick()

	SendChatMessage(gmCommands["AC"])

end	

function btngmTeleHyj_OnClick()

	SendChatMessage(gmCommands["Hyj"])

end	

function btngmTeleKazzak_OnClick()

	SendChatMessage(gmCommands["Kazzak"])

end	

function btngmTeleAzuregos_OnClick()

	SendChatMessage(gmCommands["Azuregos"])

end	
function btngmTeleLethon_OnClick()

	SendChatMessage(gmCommands["Lethon"])

end	
function btngmTeleEmeriss_OnClick()

	SendChatMessage(gmCommands["Emeriss"])

end	
function btngmTeleYsondre_OnClick()

	SendChatMessage(gmCommands["Ysondre"])

end	
function btngmTeleTaerar_OnClick()

	SendChatMessage(gmCommands["Taerar"])

end	

function btngmAuraPirateM_OnClick()

	SendChatMessage(gmCommands["PirateMale"])

end	

function btngmAuraPirateF_OnClick()

	SendChatMessage(gmCommands["PirateFemale"])

end	

function btngmAuraNinjaM_OnClick()

	SendChatMessage(gmCommands["NinjaMale"])

end	

function btngmAuraNinjaF_OnClick()

	SendChatMessage(gmCommands["NinjaFemale"])

end	

function btngmAuraLeper_OnClick()

	SendChatMessage(gmCommands["LeperGnome"])

end	

function btngmAuraSkeleton_OnClick()

	SendChatMessage(gmCommands["Skeleton"])

end	

function btngmAuraGhostM_OnClick()

	SendChatMessage(gmCommands["GhostMale"])

end	

function btngmAuraGhostF_OnClick()

	SendChatMessage(gmCommands["GhostFemale"])

end	

function btngmAuraRedOgre_OnClick()

	SendChatMessage(gmCommands["RedOgre"])

end	

function btngmAuraStunForever_OnClick()

	SendChatMessage(gmCommands["StunForever"])

end	

function btngmAuraUnStunForever_OnClick()

	SendChatMessage(gmCommands["UnStunForever"])

end	

function btngmAuraFreeze_OnClick()

	SendChatMessage(gmCommands["Freeze"])

end	

function btngmAuraUnFreeze_OnClick()

	SendChatMessage(gmCommands["UnFreeze"])

end	


function updateBtns()
	if gmTicketSys["NumberOfOnlineTickets"] > 0 then 
		gmTCount:SetText("Tickets: "..gmTicketSys["NumberOfTickets"].."("..GREEN_FONT_COLOR_CODE..gmTicketSys["NumberOfOnlineTickets"]..FONT_COLOR_CODE_CLOSE..")")
	else
		gmTCount:SetText("Tickets: "..gmTicketSys["NumberOfTickets"])
	end
	if ticketArray and btn and ticketArray[MySlider:GetValue()] then
	else
		return
	end
	
	if getn(ticketArray) > 22 then
		value = MySlider:GetValue()
		
	else
		value = 1
		
	end

	--if gmFilterOnline:GetChecked() then -- Show only online tickets (10)
	--		for i=1,getn(btn) do
	--			btn[i]:Hide()
	--		end

	--		iBtn = 1
	--		table.foreachi(ticketArray, function(k,v)
	--			if v.TicketOnline == true then
	--				if btn[iBtn] == nil then return end
	--				btn[iBtn]:SetText(GREEN_FONT_COLOR_CODE..v.playerName)
	--				if not btn[iBtn]:IsShown() then btn[iBtn]:Show() end
	--				iBtn = iBtn + 1
	--			end
	--		end)


	--else -- Show online and offlines tickets
	for i=1,getn(btn) do
		-- GREEN_FONT_COLOR_CODE
		if btn[i] and ticketArray[i-1 + value] then
			
			if ticketArray[i].answeredBy ~= ""  then
				btn[i]:SetText(GRAY_FONT_COLOR_CODE..ticketArray[i-1 + value].playerName)
			elseif ticketArray[i-1 + value].TicketOnline == true then
				btn[i]:SetText(GREEN_FONT_COLOR_CODE..ticketArray[i-1 + value].playerName)  
			else
				btn[i]:SetText(ticketArray[i-1 + value].playerName) 
			end
			
			

			if not btn[i]:IsShown() then btn[i]:Show() end
		else
			btn[i]:Hide()
		end

	end
	--end
end


function updateAnswered()


end

function gmSetTicket(i)
	

	if gmTicketSys["NumberOfTickets"] == 0 then
		gmPlayerName:SetText("")
		txtGmMsgT:SetText("It's all done! Great job!")
		txtGmTTime:SetText("")
		--MySlider:SetValue(0)
		return
	elseif not ticketArray[i] then 
		return 
	end -- /print ticketArray[3].ticketOnline

	if ticketArray[i].TicketOnline == true then
		gmStatusOnline:Hide()
	else
		gmStatusOnline:Hide()
	end
	
	-- print(ticketArray[i].answeredBy)
	if ticketArray[i].answeredBy ~= "" then
		gmAssign:SetText("This ticket has been answered by: "..ticketArray[i].answeredBy)
		gmAssignBox:Show()
		btnGmAssignBoxClose:Show()
	else
		gmAssignBox:Hide()
		gmAssign:SetText("")
		btnGmAssignBoxClose:Hide()
	end

	

	gmPlayerName:SetText(ticketArray[i].playerName)
	txtGmMsgT:SetText(ticketArray[i].ticketMsg)
	txtGmTTime:SetText("Made: "..ticketArray[i].ticketDateA.." "..ticketArray[i].ticketTime)
	--gmSendAddonMsg("view ticket")
	-- MySlider:SetValue(i)
	if gmTicketSys["ticketCurrent"] ~= i then gmTicketSys["ticketCurrent"] = i; end
end


function txtGmSendWhisp_OnEnterPressed()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	GmAddonW(txtGmSendWhisp:GetText(), gmPlayerName:GetText())
	gmClearntxtGmSendWhisp()	
end



--ilmus modifier key responses here

--ilmus random whisper button here

function txtGmSendWhisp2__OnClick()
 Min = 1
 Max = 12
	local GetRandomWhisper = random(Min,Max)
	local gmStartConversation = GetRandomWhisper
		if gmStartConversation == 1 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Salutations, Traveler "..gmPlayerName:GetText()..". I am Game Master "..UnitName("Player")..". How are you doing on this fine day?","WHISPER", nil, gmPlayerName:GetText());	
		end
		if gmStartConversation == 2 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings, "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". Do you have a moment to talk about your ticket?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 3 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings, "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". Thank you for taking the time to contact us. Have you a few moments to speak with me?","WHISPER", nil, gmPlayerName:GetText());		
		
		end
		if gmStartConversation == 4 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings, "..gmPlayerName:GetText()..". I am Game Master "..UnitName("Player")..". Do you have a moment to discuss why you have summoned me from my slumber?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 5 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Salutations, "..gmPlayerName:GetText().."! Game Master "..UnitName("Player").." reporting in. Do you have a moment to chat?","WHISPER", nil, gmPlayerName:GetText());
		
		end
		if gmStartConversation == 6 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings, my friend "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". I have received your ticket in regards to a issue you are having. Thanks for using our Ticket System. Do you have a moment to chat?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 7 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("*The ground shakes beneath your feet. An enormous lava elemental spews up from the ground.* Greetings "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". Do you have a moment to discuss why you have summoned me from my slumber?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 8 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Salutations "..gmPlayerName:GetText().."! I am Game Master "..UnitName("Player")..". My apologizes for disrupting your playtime, do you have a moment to discuss the ticket you created?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 9 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings, "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". Do you have a second to talk about your ticket?","WHISPER", nil, gmPlayerName:GetText());
		end		
		if gmStartConversation == 10 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings, my friend "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". How are you today?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 11 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings "..gmPlayerName:GetText()..", this is Game Master "..UnitName("Player")..". How are you today?","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 12 then
			gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
			SendChatMessage("Greetings "..gmPlayerName:GetText()..". This is Game Master "..UnitName("Player")..". I've received your petition and I'm here to try and help you out. But first, how are you doing today?","WHISPER", nil, gmPlayerName:GetText());
		end
end


function xtxtGmSendWhisp3__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("Greetings, "..gmPlayerName:GetText()..". This is Gamemaster "..UnitName("Player")..". Do you have a moment to talk about your ticket?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Salutations, Traveler "..gmPlayerName:GetText()..". I am Gamemaster "..UnitName("Player")..". How are you doing on this fine day?","WHISPER", nil, gmPlayerName:GetText());
	
end

function txtGmSendWhisp3__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("Salutations, Traveler "..gmPlayerName:GetText()..". I am Gamemaster "..UnitName("Player")..". How are you doing on this fine day?","WHISPER", nil, gmPlayerName:GetText());
end

function txtGmSendWhisp4__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("Greetings, "..gmPlayerName:GetText()..". This is Gamemaster "..UnitName("Player")..". Thank you for taking the time to contact us. Have you a few moments to speak with me?","WHISPER", nil, gmPlayerName:GetText());
end

function txtGmSendWhisp5__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("Greetings, My friend "..gmPlayerName:GetText()..". This is Gamemaster "..UnitName("Player")..". I have received your ticket in regards to a issue you are having. Thanks for using our Ticket System. Do you have a moment to chat?","WHISPER", nil, gmPlayerName:GetText());
end

function txtGmSendWhisp6__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("Greetings, "..gmPlayerName:GetText()..". I am Game Master "..UnitName("Player")..". Do you have a moment to discuss why you have summoned me from my slumber?","WHISPER", nil, gmPlayerName:GetText());
end

function txtGmSendWhisp7__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("Salutations, "..gmPlayerName:GetText().."! Game Master "..UnitName("Player").." reporting in. Do you have a moment to chat?","WHISPER", nil, gmPlayerName:GetText());
end

function txtGmSendJoke2__OnClick()
 Min = 1
 Max = 20
	local GetRandomWhisper = random(Min,Max)
	local gmStartConversation = GetRandomWhisper
		if gmStartConversation == 1 then
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("What do you call a druid who melees in tree form?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("A combat log.","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 2 then
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("And Christ said unto his disciples: 'I shall grant you eternal Salvation!' The disciples fell to their knees and replied: 'Give us Kings, noob!'","WHISPER", nil, gmPlayerName:GetText());
		end
		if gmStartConversation == 3 then
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("What did the main tank say to his girlfriend when she caught him raiding with a resto druid?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("''What can i say? She has the HoTs for me''","WHISPER", nil, gmPlayerName:GetText());	
		
		end
		if gmStartConversation == 4 then
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("One day Arthas was at Uther's house and asked 'What's the switch on the wall for?'","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Uther replied 'FOR THE LIGHT!'","WHISPER", nil, gmPlayerName:GetText());	
		end
		if gmStartConversation == 5 then
	SendChatMessage("Why does the world's greatest raid team smell so bad?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("They never wipe.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		
		end
		if gmStartConversation == 6 then
	SendChatMessage("What does a paladin and a chicken got incommon?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("They both BoK BoK BoK.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
		end
		if gmStartConversation == 7 then
	SendChatMessage("Why does Naxxramas fly?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("It has four wings of course.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end
		if gmStartConversation == 8 then
	SendChatMessage("Why are pallidins overpowered?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Because they come with their own Chargers.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end
		if gmStartConversation == 9 then
	SendChatMessage("Why doesn't thunder bluff have any restautants?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("The waiters keep quitting when they find out they'll be tipped.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
		end		
		if gmStartConversation == 10 then
	SendChatMessage("Do you know why gnomes can't be paladins?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Poor little guys Can't reach the light. :(","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
		end
		if gmStartConversation == 11 then
	SendChatMessage("Why was the Gnome who just joined a nudist colony asked to leave?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("because he kept poking his nose into everybody's business.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end
		if gmStartConversation == 12 then
	SendChatMessage("How many GM's does it take to change a light bulb?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("None, it's working as intended.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
		end
		if gmStartConversation == 13 then
	SendChatMessage("Why didn't the Forsaken cross the road?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("He didn't have the guts.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
		end	
		if gmStartConversation == 14 then
	SendChatMessage("What do you call it if paladins and druids share a bath?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("A HoT tub with Bubbles.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
		if gmStartConversation == 15 then
	SendChatMessage("How did the paladins get clean?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("They had a bubble bath!","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
		if gmStartConversation == 16 then
	SendChatMessage("Which dragon has the dream job?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Ysera","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
		if gmStartConversation == 17 then
	SendChatMessage("Why is the Cenarion Circle neutral?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Circles have no sides.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
		if gmStartConversation == 18 then
	SendChatMessage("Why do sneaky rogues prefer leather armor?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("because it's made of hide","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
		if gmStartConversation == 19 then
	SendChatMessage("Why are hunters bad at photography?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("They have no focus.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
		if gmStartConversation == 20 then
	SendChatMessage("What did Gul'dan do when he tripped?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("He fel..","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
		end	
end

function txtGmJoke1__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("What do you call a druid who melees in tree form?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("A combat log.","WHISPER", nil, gmPlayerName:GetText());	
end

function txtGmJoke2__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("And Christ said unto his disciples: 'I shall grant you eternal Salvation!' The disciples fell to their knees and replied: 'Give us Kings, noob!'","WHISPER", nil, gmPlayerName:GetText());
end

function txtGmJoke3__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("What did the main tank say to his girlfriend when she caught him raiding with a resto druid?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("''What can i say? She has the HoTs for me''","WHISPER", nil, gmPlayerName:GetText());	
end

function txtGmJoke4__OnClick()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("One day Arthas was at Uther's house and asked 'What's the switch on the wall for?'","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Uther replied 'FOR THE LIGHT!'","WHISPER", nil, gmPlayerName:GetText());	
end

function txtGmJoke5__OnClick()
	SendChatMessage("Why does the world's greatest raid team smell so bad?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("They never wipe.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
end

function txtGmJoke6__OnClick()
	SendChatMessage("What does a paladin and a chicken got incommon?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("They both BoK BoK BoK.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
end

function txtGmJoke7__OnClick()
	SendChatMessage("Why does Naxxramas fly?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("It has four wings of course.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
end

function txtGmJoke8__OnClick()
	SendChatMessage("Why are pallidins overpowered?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Because they come with their own Chargers.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
end

function txtGmJoke9__OnClick()
	SendChatMessage("Why doesn't thunder bluff have any restautants?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("The waiters keep quitting when they find out they'll be tipped.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
end

function txtGmJoke10__OnClick()
	SendChatMessage("Do you know why gnomes can't be paladins?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("Poor little guys Can't reach the light. :(","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
end

function txtGmJoke11__OnClick()
	SendChatMessage("Why was the Gnome who just joined a nudist colony asked to leave?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("because he kept poking his nose into everybody's business.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
end

function txtGmJoke12__OnClick()
	SendChatMessage("How many GM's does it take to change a light bulb?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("None, it's working as intended.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
end

function txtGmJoke13__OnClick()
	SendChatMessage("Why didn't the Forsaken cross the road?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("He didn't have the guts.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
end

function txtGmJoke14__OnClick()
	SendChatMessage("What do you call it if paladins and druids share a bath?","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("A HoT tub with Bubbles.","WHISPER", nil, gmPlayerName:GetText());	
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))
end

function txtGmJoke15__OnClick()
	SendChatMessage("A rough and tough veteran Tauren Warrior walks into an inn. He's a scary looking customer, grizzled, scarred and mean as a bag of ravagers. His hooves thunder on the floor boards as he swaggers up to the bar and orders a drink.","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("The rest of the patrons are silent, petrified with fear. He finishes his drink, then goes outside to leave. He returns moments later, sweeps every patron of the inn with his steely gaze, cracks his massive knuckles, and in a low, dangerous voice utters.","WHISPER", nil, gmPlayerName:GetText());	
	SendChatMessage("''I'm going to sit down and have another drink. If my Kodo isn't back where I left it by the time I'm done, I'm going to have to do what I did back in the Barrens. And I really don't want have to do what I did back in the Barrens.''","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("He sits down, and orders another drink. In time he finishes his second drink, gets up, and stalks to the door to check outside. Lo and behold, his Kodo is sitting contentedly next to the Stablemaster, right where it belongs","WHISPER", nil, gmPlayerName:GetText());	
	SendChatMessage("A satisfied sneer on his face, the Warrior prepares to leave, but before he departs the Orc Peon that was tending bar timidlly asks, ''Um, mister Warrior? What did you have to do back in the Barrens?''","WHISPER", nil, gmPlayerName:GetText());
	SendChatMessage("The grizzled veteran gets a sad, far-off look in his eyes, and rumbles ''I had to walk home.''","WHISPER", nil, gmPlayerName:GetText());		
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Whisper by "..UnitName("Player"))	
end

function txtGmCommon1__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", here is something you can try to fix your issue. Exit your game, then delete the 'WDB' cache folder located in your WoW install directory. Then launch the game again and disable ALL addons.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon2__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", gamemasters can not assist players with in-game hints or offer any help for solving quests. Why not ask your fellow players for tips and hints instead? Check out our database at: https://vanilla-twinhead.twinstar.cz","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon3__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", information regarding your ban, can be found at www.twinstar.cz by logging in to the account manager - with your WoW account.","WHISPER", nil, gmCommonPlayer:GetText());	
	SendChatMessage("Sometimes you might encounter difficulties logging in to the account manager, in this case set your browser into 'Private Browsing Mode' when possible. To disable any login cookies from interfering with old login details.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon4__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", first make sure you have all the required quest items in your inventory. Then abandon the quest (your quest items will not be destroyed in the process). After you have done that, re-accept the quest from the quest giver, and turn that in.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon5__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", thank you for your report. We will investigate this further and take appropriate action as necessary.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon6__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", you can find all of the Kronos rules by following this link: http://www.kronos-wow.com/project-kronos-rules","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon7__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", you have been invited to join our IRC help channel. Server: irc.twinstar.cz ; Port: +6697 ; SSL: On; Channel: #kronos-help - you will need an IRC client in order to join us! Some clients you can find on the internet: HexChat, IceChat, mIRC.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon8__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", please direct all ban appeals to Gurky@Twinstar.cz","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon9__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", We are aware of this issue and it is currently awaiting a fix. Visit the Twinhead Bug Tracker for more information regarding this problem: https://vanilla-twinhead.twinstar.cz/?issues","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon10__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", you can read up on how to donate by following this link: https://www.twinstar.cz/manager/Donate/DonateHelp.aspx In order to see all of the features, you will have to log in from the top right corner of the website - using your WoW account.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon11__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", in order to obtain a new Hearthstone you will have to visit an inn and talk to the innkeeper. Hold down left ctrl key, and right click the innkeeper then choose the following dialogue: 'Make this inn my home'.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon12__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", here are some methods you can try to get rid of the Combat Bug by your own: A) Press ALT+F4 to exit your game, B) Die to a monster in a PVE encounter. and C) Use the hearthstone. ","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon13__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", Here's something you can try to fix your drunken camera issue! Try relogging, if that doesn't help please just die to a monster. If after dying you still notice this effect, please relog and it should be gone.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon14__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", unfortunately we are unable to recover any loot that has gone missing during a server crash or a rollback. We are sorry for any inconvenience this has caused you.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon15__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage("On Kronos it is strictly forbidden to actively control multiple Characters at once using 3rd party software/3rd party mechanics (known as Multiboxing). It is also against the rules to participate in a Battleground with more than 1 character.","WHISPER", nil, gmCommonPlayer:GetText());	
	SendChatMessage("However Alt Tabbing with multiple clients to play (Questing, World PvP, etc…) is not against the rules - while doing this battlegrounds is against the rules!","WHISPER", nil, gmCommonPlayer:GetText());	
	SendChatMessage("Botting and/or unmanned automation of your character is strictly prohibited and will be banned on the spot.","WHISPER", nil, gmCommonPlayer:GetText());		
end

function txtGmCommon16__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", in vanilla WoW Meeting Stones do not have the ability to summon players. Meeting Stones are for filling in partial (or just-starting) parties whose members met the required level range by auto-inviting suitable candidates.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon17__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", we thank you for your report regarding the gold seller(s). And if you have not - please provide us with a screenshot! -- This is an automatic message.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon18__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", 1text here.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon19__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", we need more information. When submitting an Gamemaster ticket - please provide as much information as possible. Describe your issue shortly but clearly. When reporting players include names, what they said and screenshots as proofs.","WHISPER", nil, gmCommonPlayer:GetText());	
end

--Ilmus started adding new Whisper responses here.
function txtGmCommon20__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", the Fishing Extravaganza event takes place every sunday 15:00 - 18:00 server time! Visit our database for more information regarding this event: https://vanilla-twinhead.twinstar.cz","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon21__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", the account was automatically suspended for 10 minutes for entering the password wrong too many times. You can change your password at www.twinstar.cz in the account manager.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon22__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", one bubble of rested XP (5% of a level) is earned for every 8 hours spent resting. A maximum of 30 bubbles (150% of a level) may be earned. ","WHISPER", nil, gmCommonPlayer:GetText());	
	SendChatMessage("In other words, you can leave your character resting in an inn or capital city for up to 10 days (without gaining experience) before you earn the maximum amount of rest state.","WHISPER", nil, gmCommonPlayer:GetText());	
	SendChatMessage("And a resting character stores up one full level of rest in 160 hours (6 days, 16 hours). I hope this answers it for you!","WHISPER", nil, gmCommonPlayer:GetText());
end

function txtGmCommon23__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", one bubble of rested XP (5% of a level) is earned for every 8 hours spent resting. A maximum of 30 bubbles (150% of a level) may be earned. ","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon24__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", please use the correct format to report an loot incident via a ticket. You can find the instructions here: http://forum.twinstar.cz/showthread.php/103614-Loot-Incident-Quota-All-Loot-Issues-please-read!","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon25__OnClick()
	gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
	SendChatMessage(""..gmCommonPlayer:GetText()..", there is an outgoing mail cap per character - which is your issue here! Please log on your alt character to claim your mails from the mailbox, this will fix your issue on your main character.","WHISPER", nil, gmCommonPlayer:GetText());	
end

function txtGmCommon26__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", at the given moment there is no way to change your character's race. But it is a feature we will surely be providing to our players in the future! Once it is available, you will find it at www.twinstar.cz in the account manager.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtGmCommon27__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", unfortunately there is nothing in-game GM's are able to do about this issue at the given moment.","WHISPER", nil, gmCommonPlayer:GetText()); 
	SendChatMessage("Please report this bug to our Bugtracker at: https://vanilla-twinhead.twinstar.cz/?issues this way our developers can identify the issue and fix it!","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtGmCommon28__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", at the given moment there is no way to change your character's faction. But it is a feature we will surely be providing to our players in the future! Once it is available, you will find it at www.twinstar.cz in the account manager.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function btngmCommon29__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", you can post your ideas and feature requests to our Suggestions Forum at: http://forum.twinstar.cz/forumdisplay.php/758-Suggestions-and-comments this way our developers can hear your voice, and make great things happen!","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function btngmCommon30__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", we are very sorry to hear that you've been scammed. Unfortunately we can not restore the items/gold that you've lost. We just wanted to give you a heads up that we're investigating this report and taking appropriate action as necessary.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function btngmCommon34__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", you are able to rename your characters in the Account Manager: www.Twinstar.cz ; Renaming your character will cost you 100 Stars. I hope this will answer your question!","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtgmCommon35__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", if you are unable to turn your guild charter in to the Guild Master then here is something you can try: Please place your guild charter item into the first slot of your main bag, and try talking to the Guild Master NPC again.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtgmCommon36__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", please open your Spellbook. Take a look at the bottom frame of the Spellbook, there is a tab called 'Demon' - open it (you must have your minion summoned). Then drag the Blood Pact ability manually to your pet bar.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtgmCommon37__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", unfortunately we are unable to restore any items or gold that you lost. We can however help you to recover quest rewards, by re-adding you the quests. In this case you will have to provide us with the quest names.","WHISPER", nil, gmCommonPlayer:GetText()); 
    SendChatMessage("Please go to www.twinstar.cz account manager and change your password, or use this command in-game: .account password. It is possible that you used same login info on other private server projects.","WHISPER", nil, gmCommonPlayer:GetText()); 
    SendChatMessage("Also to be on the safe side, please do not use same password on your WoW account that you use in the forums.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtgmCommon38__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", in Vanilla WoW warlocks can not summon a player inside the instance, when the player is not inside the same instance and vice versa. You will have to move out from the instance in order to summon them.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtgmCommon39__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", you can change your password in the Account Manager on our website at: www.twinstar.cz ; additionally you can also change your password in-game with the following command: .account password oldpass newpass newpass.","WHISPER", nil, gmCommonPlayer:GetText()); 
    SendChatMessage("Do not use same account name & password from other private server projects! Keep your password different from what it is in the forums. Only you are responsible for your account's security.","WHISPER", nil, gmCommonPlayer:GetText()); 
end

function txtgmCommon40__OnClick()
    gmSendAddonMsg("Ticket from "..gmCommonPlayer:GetText().." answered via Whisper by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", at the given moment there is no way to change your character's gender. But it is a feature we will surely be providing to our players in the future! Once it is available, you will find it at www.twinstar.cz in the account manager.","WHISPER", nil, gmCommonPlayer:GetText()); 
end
--Ilmus stopped adding new Whisper responses here.   btngmMailCommon30

--Trying to add random responses




--Random response attempt stopped here

function gmClearntxtGmSendWhisp()
	txtGmSendWhisp:ClearFocus()
	txtGmSendWhisp:AddHistoryLine(txtGmSendWhisp:GetText())
	txtGmSendWhisp:SetText("")

end

function gmUpdateSlider()
	
	
	local str = ""
	--if gmFilterOnline:GetChecked() then 
	--str = "NumberOfOnlineTickets"
	--else
	str = "NumberOfTickets"
	--end

	getglobal(MySlider:GetName() .. 'High'):SetText(gmTicketSys[str]);

	if gmTicketSys[str] > 22 then
		MySlider:SetMinMaxValues(1, gmTicketSys[str]-21)
		MySlider:Show()
	else
		MySlider:SetMinMaxValues(1, gmTicketSys[str])
		MySlider:Show()
	end

	-- /print MySlider:IsToplevel()
	MySlider:SetValueStep(1)
	--MySlider:SetValue(gmTicketSys["ticketCurrent"])
	MySlider:SetValue(MySlider:GetValue())
end

function gmAddonFrame_OnUpdate()
	if ticketArray and gmPlayerName:GetText() == "" and gmTicketSys["NumberOfTickets"] > 0 then gmSetTicket(1); gmUpdateSlider();end --updateBtns() 
	if (gmAddonReadTicket ~= nil and  gmAddonReadTicket + 1 < GetTime())  then -- reading of the ticket ends with a message from different source then SYSTEM MESSAGE -- id ~= "gmAddon" and 
		gmUpdateSlider();
		gmAddonReadTicket = nil
		gmStopReadingTickets()
	end
	if gmWhoTimer and gmWhoTimer + 1 < GetTime() then
		gmSetTicket(i)
		gmUpdateSlider()
		updateBtns()
		gmWhoTimer = nil
	end

	if gmCheckOnline:GetChecked() then
		gmCheckOnlineTicket()
	end

end
-- /run simTickets()

function gmCheckOnlineTicket()
	if not ticketArray or gmTicketSys["NumberOfTickets"] == 0 then return end
	

	if not gmCheckOnlineTicketTime or not gmCheckOnlineTicketIndex then 
		gmCheckOnlineTicketTime = GetTime()
		gmCheckOnlineTicketIndex = 1
		-- return
	elseif gmCheckOnlineTicketIndex > gmTicketSys["NumberOfTickets"] then
		gmCheckOnlineTicketIndex = 1
		
	end

	if gmCheckOnlineTicketTime + 10 > GetTime() then return end -- 10s checkonline check online

	if ticketArray[gmCheckOnlineTicketIndex] then
		
		gmCheckOnlinePlayerName = ticketArray[gmCheckOnlineTicketIndex].playerName
		if ticketArray[gmCheckOnlineTicketIndex].TicketOnline == true and gmTicketSys["NumberOfOnlineTickets"] > 0 then gmTicketSys["NumberOfOnlineTickets"] = gmTicketSys["NumberOfOnlineTickets"] - 1 end
		ticketArray[gmCheckOnlineTicketIndex]:setTicketOnline(false)
		SendWho("n-\""..gmCheckOnlinePlayerName.."\"")
		-- gmAddonPrint(("/who n-\""..gmCheckOnlinePlayerName.."\""..gmCheckOnlineTicketIndex))
		gmCheckOnlineTicketIndex = gmCheckOnlineTicketIndex + 1
		gmCheckOnlineTicketTime = GetTime()
	else


	end
	
end


function btnGmOnlineT_OnClick()
	SendWho("n-\""..gmPlayerName:GetText().."\"")
	--gmTicketSys["NumberOfOnlineTickets"] = 0
	--gmWhoTimer = GetTime()
	--table.foreachi(ticketArray, function(k,v)
	--	v:setTicketOnline(false)
	--	SendWho("n-\""..v.playerName.."\"")
	--end)
end

--function gmFilterOnline_OnClick()
--	gmUpdateSlider()
--	updateBtns()
--end

function btnGmSummon_OnClick()
	SendChatMessage(gmCommands["summon"].." "..gmPlayerName:GetText())
end

function btnGmGo_OnClick()
	SendChatMessage(gmCommands["teleportTo"].." "..gmPlayerName:GetText())
	TargetByName(gmPlayerName:GetText())
end

function btnGmGoView_OnClick()
	SendChatMessage(gmCommands["setview"])
end

function btnGmSummon1_OnClick()
	SendChatMessage(gmCommands["recall"].." "..gmPlayerName:GetText())
end

function gmPlayerName_OnEnterPressed()
	SendWho("n-\""..gmPlayerName:GetText().."\"")
	gmPlayerName:ClearFocus()
end


function simTickets()
	ticketArray = {};
	for i=1, 250 do
		t = Ticket:new()
		t:setPlayerName("Player".. i)
		t:setTicketMsg(i*24)
		t:setTicketTime(12 ..":"..i)
		t:setTicketDateA(21 ..".".. 12 ..".".. 2015)
		if math.random() > 0.6 then t:setTicketOnline(true) end
		tinsert(ticketArray,t);
		gmTicketSys["NumberOfTickets"] = gmTicketSys["NumberOfTickets"] + 1
		gmUpdateSlider()

		
	end
	updateBtns()
end

function gmTurnOnOff()
	if gmAddonFrame:IsVisible() then
		gmAddonFrame:Hide()
		MySlider:Hide()
	else
		gmAddonFrame:Show()
		MySlider:Show()
	end

end


SLASH_GM1= '/gm';
SLASH_GM2= '/ti';
SLASH_GM3= '/ticket';
SLASH_GM4= '/gma';
SLASH_GM5= '/gmaddon';
SlashCmdList["GM"] = gmTurnOnOff;

function gmMailFrame_OnLoad()
	

	gmMailMsg:SetBackdrop({
		bgFile = [[Interface\Buttons\WHITE8x8]],
		insets = {left = -5, right = 0, top = 0, bottom = 0
		}})
	gmMailMsg:SetBackdropColor(0, 0, 0, 0.5)
	gmMailMsg:SetHeight(13)

end	

function gmBanFrame_OnLoad()
	

end	

function gmConfirmFrame_OnLoad()
	

end	

function gmConfirmIPFrame_OnLoad()
	

end	

function gmLearnFrame_OnLoad()
	


end

function gmCommonFrame_OnLoad()
btngmCommon33:Disable()
btngmCommon33:Disable()
btngmCommon32:Disable()


end	

function gmToggleGMFrame_OnLoad()
	


end	

function gmToggleWGMFrame_OnLoad()
	


end	

function gmToggleVGMFrame_OnLoad()
	


end	

function gmTeleFrame_OnLoad()
	


end	

function gmMaintenanceFrame_OnLoad()
	


end	

function gmAuraFrame_OnLoad()
	


end	

function gmJokeFrame_OnLoad()
	


end	
	

function gmNoteFrame_OnLoad()
	


	
	local CopyChat = CreateFrame('Frame', 'nChatCopy', UIParent)
	CopyChat:SetWidth(380)
	CopyChat:SetHeight(380)
	CopyChat:SetPoint('CENTER', gmNoteFrame, 'CENTER', 0, 0)
	CopyChat:SetFrameStrata('DIALOG')
	CopyChat:Hide()
	CopyChat:SetBackdrop({
		bgFile = [[Interface\Buttons\WHITE8x8]],
		insets = {left = 3, right = 3, top = 4, bottom = 3
		}})
	CopyChat:SetBackdropColor(0, 0, 0, 0.7)

	-- CreateBorder(CopyChat, 12, 1, 1, 1)

	CopyChatBox = CreateFrame('EditBox', 'nChatCopyBox', CopyChat)
	CopyChatBox:SetMultiLine(true)
	CopyChatBox:SetAutoFocus(false)
	CopyChatBox:EnableMouse(true)
	CopyChatBox:SetMaxLetters(99999)
	CopyChatBox:SetFont('Fonts\\ARIALN.ttf', 13, 'THINOUTLINE')
	CopyChatBox:SetWidth(350)
	CopyChatBox:SetHeight(650)
	CopyChatBox:SetScript('OnEscapePressed', function() CopyChatBox:ClearFocus() end)
	if not gmSave then gmSave = {} end
	CopyChatBox:SetScript('OnTextChanged', function() if strlen( CopyChatBox:GetText()) > 0 then gmSave["notepad"] = CopyChatBox:GetText() end end)
	
	

	local Scroll = CreateFrame('ScrollFrame', 'nChatCopyScroll', CopyChat, 'UIPanelScrollFrameTemplate')
	Scroll:SetPoint('TOPLEFT', CopyChat, 'TOPLEFT', 0, -5)
	Scroll:SetPoint('BOTTOMRIGHT', CopyChat, 'BOTTOMRIGHT', -30, 0)
	Scroll:SetScrollChild(CopyChatBox)
end

function gmMailSubject_OnTextChanged()
	if strlen( gmMailSubject:GetText()) > 0 then gmSave["subject"] = gmMailSubject:GetText() end
end

function gmMailMsg_OnTextChanged()
	if strlen( gmMailMsg:GetText()) > 0 then gmSave["msg"] = gmMailMsg:GetText() end
end

function gmMailItem_OnTextChanged()
	if strlen( gmMailItem:GetText()) > 0 then gmSave["item"] = gmMailItem:GetText() end
end

function gmBanPlayer_OnTextChanged()
	if strlen( gmBanPlayer:GetText()) > 0 then gmSave["banplayer"] = gmBanPlayer:GetText() end
end

function gmBanTime_OnTextChanged()
	if strlen( gmBanTime:GetText()) > 0 then gmSave["bantime"] = gmBanTime:GetText() end
end

function gmBanReason_OnTextChanged()
	if strlen( gmBanReason:GetText()) > 0 then gmSave["banreason"] = gmBanReason:GetText() end
end



function gmMailSend_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmSendMail()
end

function gmMailBug_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmSendBug()
end

function gmMailWDB_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmSendWDB()
end


function gmMailMissloot_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmSendMissloot()
end

function btngmBan_OnClick()
	gmBanPlayer:ClearFocus()
	gmBanTime:ClearFocus()
	gmBanReason:ClearFocus()	
	gmSendAddonMsg(""..gmBanPlayer:GetText().." was Banned by "..UnitName("Player"))
	gmBan()
	btnConfirmClose_OnClick()
end

function btngmBanIP_OnClick()
	gmBanPlayer:ClearFocus()
	gmBanTime:ClearFocus()
	gmBanReason:ClearFocus()	
	gmSendAddonMsg("IP"..gmBanPlayer:GetText().." was Banned by "..UnitName("Player"))
	gmBanIP()
	btnConfirmIPClose_OnClick()
end

--Ilmus added more answered via Mail start-- 
function gmCommonAwaitingFix_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmCommonAwaitingFix()
end

function gmCommonInvestigation_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmCommonInvestigation()
end

function gmCommonGoldSelling_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonGoldSelling()
end

function gmCommonMultiBoxing_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonMultiBoxing()
end

function gmCommonCombatBug_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonCombatBug()
end

function gmCommonHearthstone_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonHearthstone()
end

function gmCommonCamera_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonCamera()
end

function gmCommonCrash_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonCrash()
end

function gmCommonDonations_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonDonations()
end

function gmCommonRules_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonRules()
end

function gmCommonIRC_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonIRC()
end

function gmCommonSummoningStones_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonSummoningStones()
end

function gmCommonBanInfo_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonBanInfo()
end

function gmCommonAppeals_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonAppeals()
end

function gmCommonTicketDetail_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonTicketDetail()
end

function gmCommonHints_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonHints()
end

function gmCommonAbandon_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonAbandon()
end

function gmCommonRestedXP_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonRestedXP()
end

function gmCommonLootIncident_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonLootIncident()
end

function gmCommonMailCap_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonMailCap()
end

function gmCommonBugtracker_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonBugtracker()
end

function gmCommonRaceChange_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonRaceChange()
end

function gmCommonFactionChange_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonFactionChange()
end

function gmCommonGenderChange_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonGenderChange()
end

function gmCommonScammed_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonScammed()
end

function gmCommonSuggestions_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonSuggestions()
end

function gmCommonRename_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonRename()
end

function gmCommonGuildCharter_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonGuildCharter()
end

function gmCommonFishing_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonFishing()
end

function gmCommonBloodPact_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonBloodPact()
end

function gmCommonHacked_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonHacked()
end

function gmCommonAutoBans_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonAutoBans()
end

function gmCommonRitualOfSummoning_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonRitualOfSummoning()
end

function gmCommonPasswords_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	 gmCommonPasswords()
end
--Ilmus added more answered via Mail stop--

function btngmLookup_OnClick()
	gmBanPlayer:ClearFocus()
	gmBanTime:ClearFocus()
	gmBanReason:ClearFocus()	
	gmLookup()
end

function btngmBaninfo_OnClick()
	gmBanPlayer:ClearFocus()
	gmBanTime:ClearFocus()
	gmBanReason:ClearFocus()	
	gmBaninfo()
end

function btngmKick_OnClick()
	gmBanPlayer:ClearFocus()
	gmBanTime:ClearFocus()
	gmBanReason:ClearFocus()	
	gmSendAddonMsg(""..gmBanPlayer:GetText().." was Kicked by "..UnitName("Player"))
	gmKick()
end

function btngmMute_OnClick()
	gmBanPlayer:ClearFocus()
	gmBanTime:ClearFocus()
	gmBanReason:ClearFocus()	
	gmSendAddonMsg(""..gmBanPlayer:GetText().." was Muted by "..UnitName("Player"))
	gmMute()
end

function gmMailDetail_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmSendDetail()
end

function gmMailSendI_OnClick()
	gmMailSubject:ClearFocus()
	gmMailMsg:ClearFocus()
	gmMailItem:ClearFocus()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Mail by "..UnitName("Player"))
	gmSendMailI()
end

function gmMailSendDel_OnClick()
	gmMailSend_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailDetailDel_OnClick()
	gmMailDetail_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailBugDel_OnClick()
	gmMailBug_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailWDBDel_OnClick()
	gmMailWDB_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailSendDelI_OnClick()
	gmMailSendI_OnClick()
	btnGmDeleteT_OnClick()
end

--Ilmus added Del_OnClick here START
function gmMailAwaitingFixDel_OnClick()
	gmCommonAwaitingFix_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailInvestigationDel_OnClick()
	gmCommonInvestigation_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailGoldSellingDel_OnClick()
	gmCommonGoldSelling_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailMultiBoxingDel_OnClick()
	gmCommonMultiBoxing_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailCombatBugDel_OnClick()
	gmCommonCombatBug_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailHearthstoneDel_OnClick()
	gmCommonHearthstone_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailCameraDel_OnClick()
	gmCommonCamera_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailCrashDel_OnClick()
	gmCommonCrash_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailDonationsDel_OnClick()
	gmCommonDonations_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailRulesDel_OnClick()
	gmCommonRules_OnClick()
	btnGmDeleteT_OnClick()
end

function gmMailIRCDel_OnClick()
	gmCommonIRC_OnClick()
	btnGmDeleteT_OnClick()
end

function gmSummoningStonesDel_OnClick()
	gmCommonSummoningStones_OnClick()
	btnGmDeleteT_OnClick()
end

function gmBanInfoDel_OnClick()
	gmCommonBanInfo_OnClick()
	btnGmDeleteT_OnClick()
end

function gmAppealsDel_OnClick()
	gmCommonAppeals_OnClick()
	btnGmDeleteT_OnClick()
end

function gmTicketDetailDel_OnClick()
	gmCommonTicketDetail_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonHintsDel_OnClick()
	gmCommonHints_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonAbandonDel_OnClick()
	gmCommonAbandon_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonRestedXPDel_OnClick()
	gmCommonRestedXP_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonLootIncidentDel_OnClick()
	gmCommonLootIncident_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailCaptDel_OnClick()
	gmCommonMailCap_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailRaceChangeDel_OnClick()
	gmCommonRaceChange_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailFactionChangeDel_OnClick()
	gmCommonFactionChange_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailGenderChangeDel_OnClick()
	gmCommonGenderChange_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailBugtrackerDel_OnClick()
	gmCommonBugtracker_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailScammedDel_OnClick()
	gmCommonScammed_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailSuggestionsDel_OnClick()
	gmCommonSuggestions_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailRenameDel_OnClick()
	gmCommonRename_OnClick()
	btnGmDeleteT_OnClick()
end

function gmCommonMailGuildCharterDel_OnClick()
	gmCommonGuildCharter()
	btnGmDeleteT_OnClick()
end

function gmCommonFishingDel_OnClick()
	gmCommonFishing()
	btnGmDeleteT_OnClick()
end

function gmCommonBloodPactDel_OnClick()
	gmCommonBloodPact()
	btnGmDeleteT_OnClick()
end

function gmCommonHackedDel_OnClick()
	gmCommonHacked()
	btnGmDeleteT_OnClick()
end

function gmCommonAutoBansDel_OnClick()
	gmCommonAutoBans()
	btnGmDeleteT_OnClick()
end

function gmCommonRitualOfSummoningDel_OnClick()
	gmCommonRitualOfSummoning()
	btnGmDeleteT_OnClick()
end

function gmCommonPasswordsDel_OnClick()
	gmCommonPasswords()
	btnGmDeleteT_OnClick()
end
--Ilmus stopped adding Del_OnClick here  gmCommonPasswords()

function btnGmAssignBoxClose_OnClick()
	gmAssignBox:Hide()
	btnGmAssignBoxClose:Hide()
end

function btnMailClose_OnClick()
	gmMailFrame:Hide()
	gmMailItem:SetText("")	
end

function btnBanClose_OnClick()
	gmBanFrame:Hide()
	gmBanPlayer:SetText("")	
	gmBanTime:SetText("")	
	gmBanReason:SetText("")	
	btnConfirmClose_OnClick()	
end

function btnBanIPClose_OnClick()
	gmBanFrame:Hide()
	gmBanPlayer:SetText("")	
	gmBanTime:SetText("")	
	gmBanReason:SetText("")	
	btnConfirmIPClose_OnClick()	
end

function btnNoteClose_OnClick()
	gmNoteFrame:Hide()
	nChatCopy:Hide()
end

function btnToggleGM_OnClick()
	SendChatMessage(gmCommands["gmon"]);

end

function gmToggleGMOpen_OnClick()
	if gmToggleGMFrame:IsVisible() then
		btnToggleGMClose_OnClick()	
		SendChatMessage(gmCommands["gmoff"])
	else
		gmToggleGMFrame:Show()	
		SendChatMessage(gmCommands["gmon"]);
	end
end

function gmToggleWGMOpen_OnClick()
	if gmToggleWGMFrame:IsVisible() then
		btnToggleWGMClose_OnClick()	
		SendChatMessage(gmCommands["whispoff"])
	else
		gmToggleWGMFrame:Show()	
		SendChatMessage(gmCommands["whispon"]);
	end
end

function gmToggleVGMOpen_OnClick()
	if gmToggleVGMFrame:IsVisible() then
		btnToggleVGMClose_OnClick()	
		SendChatMessage(gmCommands["gmvoff"])
	else
		gmToggleVGMFrame:Show()	
		SendChatMessage(gmCommands["gmvon"]);
	end
end

function btnLearnClose_OnClick()
	gmLearnFrame:Hide()
end

function btnCommonClose_OnClick()
	gmCommonFrame:Hide()
end

function btnTeleClose_OnClick()
	gmTeleFrame:Hide()
end

function btnMaintenanceClose_OnClick()
	gmMaintenanceFrame:Hide()
end

function btnAuraClose_OnClick()
	gmAuraFrame:Hide()
end

function btnJokeClose_OnClick()
	gmJokeFrame:Hide()
end

function btnConfirmClose_OnClick()
	gmConfirmFrame:Hide()
end

function btnConfirmIPClose_OnClick()
	gmConfirmIPFrame:Hide()
end

function btnToggleGMClose_OnClick()
	gmToggleGMFrame:Hide()
end

function btnToggleWGMClose_OnClick()
	gmToggleWGMFrame:Hide()
end

function btnToggleVGMClose_OnClick()
	gmToggleVGMFrame:Hide()
end

function gmMailOpen_OnClick()
	if gmMailFrame:IsVisible() then
		btnMailClose_OnClick()
	else
		gmMailFrame:Show()
		if gmSave["subject"] and gmMailSubject:GetText() == "" then gmMailSubject:SetText(gmSave["subject"]) end
		if gmSave["msg"] and gmMailMsg:GetText() == "" then gmMailMsg:SetText(gmSave["msg"]) end
		if gmSave["item"] and gmMailItem:GetText() == "" then gmMailItem:SetText(ID) end		
	end
end

function gmBanOpen_OnClick()
	if gmBanFrame:IsVisible() then
		btnBanClose_OnClick()
		btnConfirmClose_OnClick()
	else
		gmBanFrame:Show()
		if gmSave["banplayer"] and gmBanPlayer:GetText() == "" then gmBanPlayer:SetText("PlayerName") end
		if gmSave["bantime"] and gmBanTime:GetText() == "" then gmBanTime:SetText("Time") end
		if gmSave["banreason"] and gmBanReason:GetText() == "" then gmBanReason:SetText("Reason") end		
	end
end

function gmBanIPOpen_OnClick()
	if gmBanFrame:IsVisible() then
		btnBanClose_OnClick()
		btnConfirmIPClose_OnClick()
	else
		gmBanFrame:Show()
		if gmSave["banplayer"] and gmBanPlayer:GetText() == "" then gmBanPlayer:SetText("PlayerName") end
		if gmSave["bantime"] and gmBanTime:GetText() == "" then gmBanTime:SetText("Time") end
		if gmSave["banreason"] and gmBanReason:GetText() == "" then gmBanReason:SetText("Reason") end		
	end
end

function gmNoteOpen_OnClick()
	if gmNoteFrame:IsVisible() then
		btnNoteClose_OnClick()	
		nChatCopy:Hide()		
	else
		gmNoteFrame:Show()	
		nChatCopy:Show()
		if gmSave["notepad"] and CopyChatBox:GetText() == "" then CopyChatBox:SetText(gmSave["notepad"]) end		
	end
end

function gmLearnOpen_OnClick()
	if gmLearnFrame:IsVisible() then
		btnLearnClose_OnClick()	
	else
		gmLearnFrame:Show()	
	end
end

function gmCommonOpen_OnClick()
	if gmCommonFrame:IsVisible() then
		btnCommonClose_OnClick()	
	else
		gmCommonFrame:Show()	
	end
end

function gmTeleOpen_OnClick()
	if gmTeleFrame:IsVisible() then
		btnTeleClose_OnClick()	
	else
		gmTeleFrame:Show()	
	end
end

function gmMaintenanceOpen_OnClick()
	if gmMaintenanceFrame:IsVisible() then
		btnMaintenanceClose_OnClick()	
	else
		gmMaintenanceFrame:Show()	
	end
end

function gmAuraOpen_OnClick()
	if gmAuraFrame:IsVisible() then
		btnAuraClose_OnClick()	
	else
		gmAuraFrame:Show()	
	end
end

function gmJokeOpen_OnClick()
	if gmJokeFrame:IsVisible() then
		btnJokeClose_OnClick()	
	else
		gmJokeFrame:Show()	
	end
end

function gmConfirmOpen_OnClick()
	if gmConfirmFrame:IsVisible() then
		btnConfirmClose_OnClick()	
	else
		gmConfirmFrame:Show()	
	end
end

function gmConfirmIPOpen_OnClick()
	if gmConfirmIPFrame:IsVisible() then
		btnConfirmIPClose_OnClick()	
	else
		gmConfirmIPFrame:Show()	
	end
end



function gmPlayerName_OnTextChanged()
	gmMailPlayer:SetText(gmPlayerName:GetText())
	gmJokePlayer:SetText(gmPlayerName:GetText())
	gmCommonPlayer:SetText(gmPlayerName:GetText())	
end


function gmSendMail()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		local item = gmMailItem:GetText()		
		
		if strlen(text) <= 200 then
			SendChatMessage(gmCommands["send items"].." "..nick..' "'..subject..'" "'..text..'" "'..item..'"');
		elseif strlen(text) > 200 and strlen(text) <= 400 then
			local text1 = gmCommands["send items"].." "..nick..' "'..subject..' 1/2" "'..strsub(text,1,200)..'" "'..item..'"'
			local text2 = gmCommands["send items"].." "..nick..' "'..subject..' 2/2" "'..strsub(text,200,strlen(text))..'"'
			SendChatMessage(text1)
			SendChatMessage(text2)
		elseif strlen(text) > 200 and strlen(text) <= 600 then
			local text1 = gmCommands["send items"].." "..nick..' "'..subject..'  1/3" "'..strsub(text,1,200)..'" "'..item..'"'
			text2 = gmCommands["send items"].." "..nick..' "'..subject..' 2/3" "'..strsub(text,200,400)..'"'
			text3 = gmCommands["send items"].." "..nick..' "'..subject..' 3/3" "'..strsub(text,400,600)..'"'
			SendChatMessage(text1)
			SendChatMessage(text2)
			SendChatMessage(text3)
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmBan()
	if gmBanPlayer:GetText() ~= "" then
		local banreason = gmBanReason:GetText()
		local banplayer = gmBanPlayer:GetText()
		local banttime = gmBanTime:GetText()

			SendChatMessage(gmCommands["Ban"].." "..banplayer..' "'..banttime..'" "'..banreason..'"');
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmBanPlayer:ClearFocus()
		gmBanTime:ClearFocus()
		gmBanReason:ClearFocus()		
end

function gmBanIP()
	if gmBanPlayer:GetText() ~= "" then
		local banreason = gmBanReason:GetText()
		local banplayer = gmBanPlayer:GetText()
		local banttime = gmBanTime:GetText()

			SendChatMessage(gmCommands["BanIP"].." "..banplayer..' "'..banttime..'" "'..banreason..'"');
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmBanPlayer:ClearFocus()
		gmBanTime:ClearFocus()
		gmBanReason:ClearFocus()		
end

function gmLookup()
	if gmBanPlayer:GetText() ~= "" then
		local banreason = gmBanReason:GetText()
		local banplayer = gmBanPlayer:GetText()
		local banttime = gmBanTime:GetText()

			SendChatMessage(gmCommands["Lookup"].." "..banplayer..' ');
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmBanPlayer:ClearFocus()
		gmBanTime:ClearFocus()
		gmBanReason:ClearFocus()		
end

function gmKick()
	if gmBanPlayer:GetText() ~= "" then
		local banreason = gmBanReason:GetText()
		local banplayer = gmBanPlayer:GetText()
		local banttime = gmBanTime:GetText()

			SendChatMessage(gmCommands["Kick"].." "..banplayer..' "');
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmBanPlayer:ClearFocus()
		gmBanTime:ClearFocus()
		gmBanReason:ClearFocus()		
end

function gmMute()
	if gmBanPlayer:GetText() ~= "" then
		local banreason = gmBanReason:GetText()
		local banplayer = gmBanPlayer:GetText()
		local banttime = gmBanTime:GetText()

			SendChatMessage(gmCommands["Mute"].." "..banplayer..' '..banttime..' "'..banreason..'"');
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmBanPlayer:ClearFocus()
		gmBanTime:ClearFocus()
		gmBanReason:ClearFocus()		
end

function gmBaninfo()
	if gmBanPlayer:GetText() ~= "" then
		local banreason = gmBanReason:GetText()
		local banplayer = gmBanPlayer:GetText()
		local banttime = gmBanTime:GetText()

			SendChatMessage(gmCommands["BanInfo"].." "..banplayer..' ');
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmBanPlayer:ClearFocus()
		gmBanTime:ClearFocus()
		gmBanReason:ClearFocus()		
end



function gmSendMailI()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 200 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "'..subject..'" "'..text..'"');
		elseif strlen(text) > 200 and strlen(text) <= 400 then
			local text1 = gmCommands["send mail"].." "..nick..' "'..subject..' 1/2" "'..strsub(text,1,200)..'"'
			local text2 = gmCommands["send mail"].." "..nick..' "'..subject..' 2/2" "'..strsub(text,200,strlen(text))..'"'
			SendChatMessage(text1)
			SendChatMessage(text2)
		elseif strlen(text) > 200 and strlen(text) <= 600 then
			local text1 = gmCommands["send mail"].." "..nick..' "'..subject..' 1/3" "'..strsub(text,1,200)..'"'
			text2 = gmCommands["send mail"].." "..nick..' "'..subject..' 2/3" "'..strsub(text,200,400)..'"'
			text3 = gmCommands["send mail"].." "..nick..' "'..subject..' 3/3" "'..strsub(text,400,600)..'"'
			SendChatMessage(text1)
			SendChatMessage(text2)
			SendChatMessage(text3)
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmSendBug()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()	
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Bug Tracker" "Greetings '..nick..'.\n\nPlease use our Bug Tracker at:\n\nVanilla-Twinhead.twinstar.cz/?issues\n\nYou will need to search for the Quest/Npc/Item/etc which you find is bugged. Then vote up on this bug.\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmSendWDB()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()	
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "WDB Folder" "Greetings '..nick..'!\n\n\nHere is something you can try to fix the issue you are having.\n\nFirst close your game, and then delete the WDB folder located in your WoW directory.\n\nThen relaunch your game.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmSendMissloot()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()	
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Loot Issue" "Greetings, Thank you for the report. However for loot issues you need to use the Loot Incident Quota format located in the Raid/Dungeon forum. Please resubmit a new ticket using this format. "');		
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

--Ilmus started here to add some more mail responses
function gmCommonAwaitingFix()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Awaiting Fix" "Greetings '..nick..'!\n\n\nWe are aware of this issue and it is currently awaiting a fix.\n\n\nPlease visit our bugtracker for more information regarding this bug at:\n\nVanilla-Twinhead.twinstar.cz/?issues\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonInvestigation()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Investigation" "Greetings '..nick..'.\n\n\nThank you for your report.\n\nWe wanted to give you a heads up that your reported issue is now under our investigation.\n\n\n\nThank you for making Kronos a better place for everyone!\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonGoldSelling()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Gold Sellers" "Greetings '..nick..'!\n\n\nThank you for your report!\n\nWe wanted to let you know that we have taken action against the reported player.\n\n\nThank you making Kronos a better place for everyone!\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonMultiBoxing()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Multiboxing" "Greetings '..nick..'.\n\n\nAll the information regarding your question about Multiboxing can be found on our website!\n\n\Kronos-wow.com/Project-kronos-rules\n\n\nParagraph C. answers your question.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonCombatBug()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Combat Bug" "Greetings '..nick..'.\n\nBelow are some methods you can try to rid yourself from the Combat Bug\n\nA) Die to a monster.\nB) Press ALT+F4\nC) Use hearthstone.\n\n\nBug Tracker\n\nhttps://twinhead.twinstar.cz/?issues\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonHearthstone()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Hearthstone" "Greetings '..nick..'.\n\nTo obtain a new Hearthstone you will have to talk to an innkeeper.\n\nHold down left ctrl key, and right click the innkeeper then choose the following dialogue: Make this inn my home.\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonCamera()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Camera" "Greetings '..nick..'.\n\n\nBelow are some methods you can try to remove the Drunken Camera effect\n\nA) Relog\nB) Die to a monster\nC) Press ALT+F4\n\n\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonCrash()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Server Crash" "Greetings '..nick..'.\n\n\nUnfortunately we are unable to recover any loot that has gone missing during a server crash or a rollback.\n\n\n\nWe are sorry for any inconvenience this may have caused you.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonDonations()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Donations" "Greetings '..nick..'.\n\n\nYou can read on how to donate via:\n\nhttps://www.twinstar.cz/manager/Donate/DonateHelp.aspx\n\n\nYou will have to log in to the website from top right corner using your WoW account.\n\n\n\n\Support"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonRules()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Rules" "Greetings '..nick..'.\n\n\nYou can read about all the rules that you need to know about, on how to play safely in Kronos - on our website!\n\nKronos-wow.com/project-kronos-rules\n\n\n\n\nSafe travels!\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonIRC()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "IRC" "'..nick..'.\n\nYou have been invited to join our support channel in IRC!\n\nInformation to get you started:\n\nServer: irc.twinstar.cz\nPort: 6697\nSSL: On\nChannel: #kronos-help\n\nIRC Clients: HexChat, IceChat, mIRC\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonSummoningStones()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Meeting Stones" "Greetings '..nick..'.\n\n\In vanilla WoW Meeting Stones do not have the ability to summon players. Meeting Stones are for auto-inviting suitable candidates.\n\nMore info: \n\n\nwww.wowpedia.org/Meeting_Stone\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonBanInfo()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Ban Info" "Greetings '..nick..'.\n\n\Info regarding your ban can be found at: www.twinstar.cz\n\nLog in to the account manager - with your WoW account.\n\n\nWhen facing issues, set your browser into Private Browsing Mode.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonAppeals()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Ban Appeal" "Greetings '..nick..'.\n\n\n\n\Please direct all ban appeals to:\n\nGurky@Twinstar.cz\n\n\n\n\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonTicketDetail()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "More Info" "Greetings '..nick..'\n\nWe need more information regarding your ticket.\n\nWhen submitting an ticket, provide as much information as possible. Include player names, links to screenshots, or other curcial information."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonHints()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Hints" "Greetings '..nick..'.\n\n\Gamemasters do not assist players with in-game hints. Use the internet resources available, ask your fellow players for tips and hints!\n\nHere is our database\n\nVanilla-Twinhead.twinstar.cz\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonAbandon()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Quest" "Greetings '..nick..'.\n\n\Make sure you have the required quest items in your inventory.\n\nThen abandon the quest - this will not destroy the items.\n\nAfter you have done that, re-take the quest, and turn that in!\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonRestedXP()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Rested XP" "Greetings '..nick..'.\n\n\Here are the facts!\n\n\n1. One bubble of rested XP is earned every 8h spent resting.\n2. A maximum of 150% of a level may be earned.\n3. It takes 6D & 16H to become fully rested.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonLootIncident()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Loot Issue" "Greetings '..nick..'.\n\n\Thank you for the report.\n\nFor loot issues you need to use the Loot Incident Quota format located in the Raid/Dungeon subforum. Please resubmit your ticket using the correct format.\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonMailCap()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Mail Cap" "Greetings '..nick..'.\n\n\there is an outgoing mail cap per character.\n\n\Please log on your alt character to claim your mails from the mailbox.\n\n\n\This will fix your issue on your main character.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonBugtracker()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Bugtracker" "TXT HERE"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonRaceChange()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Race Change" "Greetings '..nick..'.\n\nCurrently there is no way to change a character\'\s race.\n\nIt is something we want to provide in the future.\n\nOnce available you will find it on the website at www.twinstar.cz\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonFactionChange()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Faction Change" "Greetings '..nick..'.\n\nCurrently there is no way to change a one\'\s faction.\n\nIt is something we want to provide in the future.\n\nOnce available you will find it on the website at www.twinstar.cz\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonGenderChange()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Gender Change" "Greetings '..nick..'.\n\nCurrently there is no way to change a one\'\s gender.\n\nIt is something we want to provide in the future.\n\nOnce available you will find it on the website at www.twinstar.cz\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonScammed()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Scammed" "Greetings '..nick..'.\n\nWe are sorry to hear that you\'\ve been scammed. However we can not restore the items/gold that you lost.\n\nWe are investigating this report and taking appropriate action as necessary.\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonSuggestions()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()

		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Suggestions" "Greetings '..nick..'.\n\nYou can post your ideas and feature requests to our Suggestions Forum at\n\nforum.twinstar.cz\n\n\nThis way our developers can hear your thoughts and make great things happen!\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonRename()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()

		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Character Rename" "Greetings '..nick..'.\n\nYou are able to rename your character at www.Twinstar.cz\n\nLog in with your WoW credentials to the site.\n\n\nRenaming a character will cost 100 Stars.\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonGuildCharter()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()

		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Guild Charter" "'..nick..'.\n\nIf you are having trouble turning your guild charter in to the Guild Master NPC.\n\nThen please place the Guild Charter item (with signatures) into the First Slot of your Main Bag.\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonFishing()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Fishing" "'..nick..'.\n\nFishing Extravaganza event takes place every sunday 15:00-18:00 server time!\n\n\nMore information:\n\nhttps://vanilla-twinhead.twinstar.cz/?event=301\n\n\nSupport."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmDiscord()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
	SendChatMessage(gmCommands["send mail"].." "..nick..' "Discord" "Greetings '..nick..'!\n\n\nYour ticket needs more help than a normal GM can provide.\n\n\nPlease contact Gurky or Davros on Discord, the link to join can be found at:\n\nwww.kronos-wow.com/support/\n\n\n\n\nSupport"');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonBloodPact()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Blood Pact" "'..nick..'.\n\nFirst open your Spellbook, then look at the bottom of the frame - there is a tab called Demon.\n\nOpen it and drag the Blood Pact ability from there manually to your pet bar.\n\n\n\n\nSupport."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonHacked()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Hacked Account?" "'..nick..'.\n\nUnfortunately we can not recover any lost gold or items. Your report is now under investigation.\n\nPlease change your account password.\n\nWe can help with restoring quest rewards.\n\n\n\n\nSupport."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonAutoBans()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Auto Bans" "'..nick..'.\n\nYour account was automatically suspended for entering the password wrong too many times.\n\nThese auto-bans last for 10 minutes.\n\n\n\n\nSupport."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonRitualOfSummoning()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Ritual of Summoning" "'..nick..'.\n\nIn Vanilla WoW it is not possible for Warlocks to summon a player inside the instance - from outside of the instance, and vice versa.\n\n\n\n\n\n\nSupport."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

function gmCommonPasswords()
	if gmMailPlayer:GetText() ~= "" then
		local text = gmMailMsg:GetText()
		local nick = gmMailPlayer:GetText()
		local subject = gmMailSubject:GetText()
		
		if strlen(text) <= 800 then
			SendChatMessage(gmCommands["send mail"].." "..nick..' "Passwords" "'..nick..'.\n\nChange your password in the Account Manager at: www.twinstar.cz\n\nOr in-game with this command:\n.account password oldpw newpw newpw\n\n\n\nSupport."');
		else
			gmAddonPrint("Message is too long!")
		end
		
		-- gmMailMsg:AddHistoryLine(gmMailMsg:GetText())
		-- gmMailMsg:SetText("")
		-- gmMailSubject:SetText("")
		gmMailMsg:ClearFocus()
	else
		
	end
end

--Ilmus stopped adding mail responses here

function btnGmRespond_OnClick()

	SendChatMessage(gmCommands["ticket respond"].." "..gmPlayerName:GetText().." "..txtGmSendWhisp:GetText())
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Respond by "..UnitName("Player"))
	gmAddonPrint("Response \""..txtGmSendWhisp:GetText().."\" added. Be aware, a player can see this while editing the ticket.")
	gmClearntxtGmSendWhisp()
	
end

function btnGmEscalate_OnClick()

	SendChatMessage(gmCommands["ticket respond"].." "..gmPlayerName:GetText().." Your ticket has been escalated to a Higher GM, by <GM> "..UnitName("Player"))
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Respond by "..UnitName("Player"))
	gmAddonPrint("Escalation added. Be aware, a player can see this escalation while editing the ticket.")
	
end

function btnGmEscalate2_OnClick()

	gmDiscord()
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Respond by "..UnitName("Player"))
    SendChatMessage(""..gmCommonPlayer:GetText()..", Your ticket needs more help than a normal GM can provide. Please contact Gurky or Davros on Discord, the link to join can be found at: www.kronos-wow.com/support/.","WHISPER", nil, gmCommonPlayer:GetText());	
	btnGmDeleteT_OnClick()
end

function btnGmHoldTicket_OnClick()

	SendChatMessage(gmCommands["ticket respond"].." "..gmPlayerName:GetText().." This ticket requires the player to come online in order to solve the issue they are having - <GM> "..UnitName("Player"))
	gmSendAddonMsg("Ticket from "..gmPlayerName:GetText().." answered via Respond by "..UnitName("Player"))
	gmAddonPrint("This ticket has now been placed on Hold.")
	
end


function gmAddonPrint(msg)
	if not msg then msg = "" end
	DEFAULT_CHAT_FRAME:AddMessage(RED_FONT_COLOR_CODE.."GM addon: "..NORMAL_FONT_COLOR_CODE..msg)
end
function gmCheckOnline_OnClick()
	if gmCheckOnline:GetChecked() then gmAddonPrint("Checking online status (using /who, 1ticket per 10s)") end
end



function gmAddonFrame_OnEvent(event, arg1,arg2,arg3,arg4,...)
	if event == "CHAT_MSG_ADDON" and arg1 ==  "gmAddon"  and  arg4 ~= UnitName("player") and ticketArray then
		-- print(arg2)
		if strfind(arg2,"Character %a+ ticket deleted") then
			-- print(arg2)
			local _,_,ticketName =  strfind(arg2,"Character (%a+) ticket deleted")
			-- print("ticket from "..ticketName.." deleted by "..arg4)
			gmDeleteTicket(ticketName)
			gmAddonPrint("Ticket from "..ticketName.." deleted by "..arg4..".");

		elseif strfind(arg2, "Ticket from %a+ answered via %a+ by %a+") then
			local _,_, ticketName, answerType, gmName = strfind(arg2, "Ticket from (%a+) answered via (%a+) by (%a+)")
			
			
			--print(ticketName)
			--print(answerType)
			--print(gmName)
			table.foreachi(ticketArray, function(k,v)
				if (v and v.playerName == ticketName) then
					if not strfind(v.answeredBy, gmName) then gmAddonPrint(arg2.."."); end
					v.answeredBy = gmName.." ("..answerType..")"
					
					if k == gmTicketSys["ticketCurrent"] then
						gmSetTicket(gmTicketSys["ticketCurrent"])
					end
					updateBtns()
					return
				end

			end)
			
			
		end
	elseif event == "CHAT_MSG_WHISPER" then
		if not autoWhispEnabled or ticketArray == nil then return end
		
		local hasTicket = false
		table.foreachi(ticketArray, function(k,v)
			-- Does he have a ticket?
			if (v and v.playerName == arg2) then
				hasTicket = true
				return
			end
		end)
		
		if (not hasTicket) then
			-- print(arg2.." doesn't have a ticket!")
			if (playerWhispArray == nil) then playerWhispArray = {} end -- This array holds information about players and their whispers
			
			local p = nil -- To hold PlayerWhisp object			
			table.foreachi(playerWhispArray, function(k,v)
				if (v and v.playerName == arg2) then
					-- "I already sent the "SYSTEM: ..." msg to this player, update his LastWhisp time"
					tremove(playerWhispArray,k)
					p = v
					p:setLastWhisp(time())
				else
					if v ~= nil and v:SendWhisp() then -- Remove old players
						tremove(playerWhispArray,k)
					end
				end
			end)
			
			if (p == nil) then
				p = PlayerWhisp:new()
				p:setPlayerName(arg2)
				p:setLastWhisp(time())
			end
			
			tinsert(playerWhispArray, p)
			if p:SendWhisp() then
				-- print("Sending whisper..."..tostring(p:SendWhisp()))
				GmAddonW("SYSTEM: This Game Master does not currently have an open ticket from you and did not receive your whisper. Please submit a new GM Ticket request if you need to speak to a GM. This is an automatic message.", p.playerName)
				p:setAutoResponseTime(time())
			end
		end
	end
end

-- /run SendAddonMessage( "gmAddon", "User Data: XYZ", "GUILD" );

function gmSendAddonMsg(msg)
	if IsInGuild() then SendAddonMessage( "gmAddon", msg, "GUILD" ); end
end

function gmTickets_OnMouseWheel(arg1,arg2)
if not arg1 then return end
 -- print(arg1)
-- print(arg2)
MySlider:SetValue(MySlider:GetValue() - arg1)

end

-- add this to your SavedVariables or as a separate SavedVariable to store its position
gmAddonFrame_Settings = {
	MinimapPos = 45 -- default position of the minimap icon in degrees
}

-- Call this in a mod's initialization to move the minimap button to its saved position (also used in its movement)
-- ** do not call from the mod's OnLoad, VARIABLES_LOADED or later is fine.
function gmAddonFrame_MinimapButton_Reposition()
	gmAddonFrame_MinimapButton:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(gmAddonFrame_Settings.MinimapPos)),(80*sin(gmAddonFrame_Settings.MinimapPos))-52)
end

-- Only while the button is dragged this is called every frame
function gmAddonFrame_MinimapButton_DraggingFrame_OnUpdate()

	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()

	xpos = xmin-xpos/UIParent:GetScale()+70 -- get coordinates as differences from the center of the minimap
	ypos = ypos/UIParent:GetScale()-ymin-70

	gmAddonFrame_Settings.MinimapPos = math.deg(math.atan2(ypos,xpos)) -- save the degrees we are relative to the minimap center
	gmAddonFrame_MinimapButton_Reposition() -- move the button
end

-- Put your code that you want on a minimap button click here.  arg1="LeftButton", "RightButton", etc
function gmAddonFrame_MinimapButton_OnClick()
	gmAddonFrame_Toggle();
end

function gmAddonFrame_Toggle()
	if(gmAddonFrame:IsVisible()) then
		gmAddonFrame:Hide();
	else
		gmAddonFrame:Show();
	end	
end


-------------------------------------------------------------
-------------------- Guild note Tickets: --------------------
-------------------------------------------------------------
gmTicketsNote = "Tickets: "
--[[  
This needs informations that client recieves after calling GuildRoster() (information about guild members)
I am now calling GuildRoster() only in btnGmLoadT_OnClick(). It might cause some problems if there are changes in the guild between 
btnGmLoadT_OnClick() and btnGmDeleteT_OnClick()
]]
-- Finds index and public note of my character in guild roster
function gmFindMyGuildIndex()
	for i = 1, GetNumGuildMembers() do
		fullName, _, _, _, _, _, note, officernote, online= GetGuildRosterInfo(i)
		-- print(fullName..": "..note)
		if fullName == UnitName("Player") then
			return i, note;
		end
	end
end

-- Removes text "Tickets: xx" from the String
function gmRemoveTickets(note)
	return string.gsub(note,gmTicketsNote.."(%d+)","" );
end

function gmAddTicketSolved() -- /run gmAddTicketSolved()
	local index, note = gmFindMyGuildIndex()
		
	if strfind(note, gmTicketsNote.."%d+") then -- If public guild note contains Tickets: xx
		local _,_,c = string.find(note, gmTicketsNote.."(%d+)") -- Parse number of tickets
		c = c + 1 -- Add one ticket
		note = gmRemoveTickets(note)
		GuildRosterSetPublicNote(index, gmTicketsNote..c..""..note) -- Add new "Tickets: xx" at the beginning of the note
	else 
		GuildRosterSetPublicNote(index, gmTicketsNote.."1 "..note) -- Tickets: 1
	end
end

-- Removes "Tickets: xx" from my guild note
function gmResetPublicNote()
	local index, note = gmFindMyGuildIndex()
	note = gmRemoveTickets(note)
	GuildRosterSetPublicNote(index, note) -- Set note without ticket info
end

-- Removes "Tickets: xx" from ALL guild notes
function gmResetPublicNotes()
	print("Removing tickets from public notes...")
	for i = 1, GetNumGuildMembers() do
		fullName, _, _, _, _, _, note, officernote, online= GetGuildRosterInfo(i)
		if strfind(note,gmTicketsNote) then
			print(fullName..": "..note.."; "..officernote)
			note = gmRemoveTickets(note)
			GuildRosterSetPublicNote(i, note) -- Set note without ticket info
		end
	end
	print("Done!")
end

-- Shows in chat guild members with note with solved tickets. GuildRoster() needs to be called before use, or just open guild window.
function gmPrintGuildNotes()
	print("List of GMs:")
	for i = 1, GetNumGuildMembers() do
		fullName, _, _, _, _, _, note, officernote, online= GetGuildRosterInfo(i)
		if strfind(note,gmTicketsNote) then
			print(fullName..": "..note.."; "..officernote)
		end
	end
end

--------------------------------------------------------------
------------------------- AUTO WHISP -------------------------
--------------------------------------------------------------

PlayerWhisp = {
	playerName = "",
	lastWhisp = 0, -- Time of last whisper from the player
	autoResponseTime = 0, -- Time of the last auto reponse msg sent to this player
	
	setPlayerName = function (self,str)
		self.playerName = str;
	end,

	setLastWhisp = function (self,str)
		self.lastWhisp = str;
	end,

	setAutoResponseTime = function (self,str)
		self.autoResponseTime = str;
	end
}

function PlayerWhisp:new(o)
	o = o or {}   -- create object if user does not provide one
	setmetatable(o, self)
	self.__index = self
	return o
end

-- Calculates if addon should send "SYSTEM: ..." msg to the player
function PlayerWhisp:SendWhisp()
	if time() > self.autoResponseTime + AUTO_WHISPER_COOLDOWN then
		return true
	end
	return false
end

function toggleAutoWhisp()
	if autoWhispEnabled == nil or autoWhispEnabled == false then
		autoWhispEnabled = true
		print("AutoWhisp: "..GREEN_FONT_COLOR_CODE.."ENABLED")
	else
		autoWhispEnabled = false
		print("AutoWhisp: "..RED_FONT_COLOR_CODE.."DISABLED")
	end
end

-- Visual *flash test for future use
function btnGmLoadT_FlashEffect()
	btnGmLoadT:Show();
	UIFrameFlash(btnGmLoadT, 2.7, 1.2, 2, 1);
	
end

