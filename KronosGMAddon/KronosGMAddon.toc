## Interface: 11200
## Title: |cFFFF7D0AKronos |cFFFFFFFFGM |cFFFF7D0AAddon
## Notes: Kronos GM Helper.
## Author: Pick, Gurky, Ilmus, Brachial
## Version: 2.3
## Dependencies: Ace2
## SavedVariables: gmSave, gmAddonFrame_Settings

libs\AceHook-2.1\AceHook-2.1.lua
gmAddonFrameForm.xml
Frame.xml
